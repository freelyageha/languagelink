# requirements
# easy-thumbnails
# django-social-auth
# django-debug-toolbar
# django-bootstrap-toolkit
# pillow
# python-dateutil

# settings.py
from os import path
import sys                                      
TIME_ZONE = 'Asia/Seoul'
PROJECT_HOME = path.abspath(path.join(path.dirname(__file__), '..'))
MEDIA_ROOT = path.join(PROJECT_HOME, "lang_media")
MEDIA_URL = '/lang_media/'
LANGUAGE_CODE = 'en'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': path.join(PROJECT_HOME, 'lang.db'),
        # The following settings are not used with sqlite3:
        'USER': '',
        'PASSWORD': '',
        'HOST': '',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',                      # Set to empty string for default.
    }
}

INSTALLED_APPS += (
    'django.contrib.admin',
    'lang',
    'bootstrap_toolkit',
    'debug_toolbar',
    'social_auth',
    'easy_thumbnails',
    'emailconfirmation',
    'compressor',
    #'south',
    )

STATICFILES_FINDERS += (
    'compressor.finders.CompressorFinder',
)

COMPRESS_ENABLED = True
COMPRESS_STORAGE = 'compressor.storage.GzipCompressorFileStorage'
COMPRESS_CSS_HASHING_METHOD = 'hash' # not using mtime since it differs between servers.

AUTHENTICATION_BACKENDS = (
    'social_auth.backends.facebook.FacebookBackend',
    )

SOCIAL_AUTH_PIPELINE = (
        'social_auth.backends.pipeline.social.social_auth_user',
        #'social_auth.backends.pipeline.associate.associate_by_email',
        'social_auth.backends.pipeline.user.get_username',
        'social_auth.backends.pipeline.user.create_user',
        'social_auth.backends.pipeline.social.associate_user',
        'social_auth.backends.pipeline.user.update_user_details',
        'social_auth.backends.pipeline.social.load_extra_data',

        'lang.pipelines.get_user_avatar',
    )

SOCIAL_AUTH_USER_MODEL = "lang.UserProfile"
FACEBOOK_EXTRA_DATA = [
        ('gender', 'gender'),
        ('birthday', 'birthday'),
        ('first_name', 'first_name'),
        ('last_name', 'last_name'),
        ]
SOCIAL_AUTH_PROTECTED_USER_FIELDS = ['email',]

#FACEBOOK_APP_ID              = '474110339343450'
FACEBOOK_APP_ID              = '248984121813216'
#FACEBOOK_API_SECRET          = '746f6b80c88639533e68f37ea8b216bd'
FACEBOOK_API_SECRET          = '3467d73dcc1573bd8d0b9461584ff223'

LOGIN_REDIRECT_URL = '/accounts/redirect/'

FACEBOOK_AUTH_EXTRA_ARGUMENTS = {'display': 'popup'}
FACEBOOK_EXTENDED_PERMISSIONS = ['email']

THUMBNAIL_ALIASES = {
    '': {
        'avatar': {'size': (80, 80), 'crop': True},
        'post': {'size': (64, 64), 'crop': True},
        'correction': {'size': (32, 32), 'crop': True},
    },
}

EMAIL_HOST_USER = 'admin@lingring.net'
NOTIFICATION_EMAIL = 'notification@lingring.net'
DEFAULT_FROM_EMAIL = 'LingRing <no-reply@lingring.net>'
EMAIL_CONFIRMATION_DAYS = 1
SERVICE_DOMAIN = 'www.lingring.net'

from django.conf import global_settings
TEMPLATE_CONTEXT_PROCESSORS = global_settings.TEMPLATE_CONTEXT_PROCESSORS + (
    'lang.context_processors.get_contexts',
)

MIDDLEWARE_CLASSES += (
    'lang.middleware.LocaleMiddleware',  
)

# urls.py
from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^', include('lang.urls')),
    url(r'^oauth/', include('social_auth.urls')),
    url(r'^accounts/', include('lang.auth_urls')),
    url(r'^admin/', include(admin.site.urls)),
)
