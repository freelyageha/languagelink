from django.contrib import admin
from lang.models import (
        Ask, Sentence, UserProfile, ChatRoom, Ring, Language,
        QuizBook, Poll, Event, SNS, Invite, Bookmark, Topic
        )

class UserProfileAdmin(admin.ModelAdmin):
    list_display = (
            'screen_name', 'email',
            'date_joined','last_login',
            'get_post_count', 'get_correction_count',
            'get_native_language', 'get_learning_language',
            'is_confirmed_email', 'sns'
            )
    search_fields = (
            'screen_name', 'email',
            )
admin.site.register(UserProfile, UserProfileAdmin)

class SNSAdmin(admin.ModelAdmin):
    list_display = ('userprofile', 'kakao', 'line', 'skype', 'message')
admin.site.register(SNS, SNSAdmin)

class AskAdmin(admin.ModelAdmin):
    list_display = (
            'get_summary', 'get_corrections',
            'user', 'language', 'get_elapsed_time', 'topic'
            )
    list_filter = ('language', )
admin.site.register(Ask, AskAdmin)

class SentenceAdmin(admin.ModelAdmin):
    list_display = ('id', 'context', )
admin.site.register(Sentence, SentenceAdmin)

class ChatRoomAdmin(admin.ModelAdmin):
    list_display = ('id', 'members', 'get_message_count', 'get_latest_message')
admin.site.register(ChatRoom, ChatRoomAdmin)

class RingAdmin(admin.ModelAdmin):
    list_display = ('tutor', 'student', 'status', 'ctime', )
admin.site.register(Ring, RingAdmin)

class QuizBookAdmin(admin.ModelAdmin):
    list_display = ('user', 'date', 'score', 'get_quiz_of_count')
admin.site.register(QuizBook, QuizBookAdmin)

class LanguageAdmin(admin.ModelAdmin):
    list_display = ('name', 'native_users', 'learning_users', 'posts', )
admin.site.register(Language, LanguageAdmin)

class EventAdmin(admin.ModelAdmin):
    list_display = ('id', 'active', )
admin.site.register(Event, EventAdmin)

class PollAdmin(admin.ModelAdmin):
    list_display = ('user', 'event_id', 'trans_context', 'ctime', )
admin.site.register(Poll, PollAdmin)

class InviteAdmin(admin.ModelAdmin):
    list_display = ('user', 'to', 'is_visited', 'is_joined', 'ctime' )
admin.site.register(Invite, InviteAdmin)

class BookmarkAdmin(admin.ModelAdmin):
    list_display = ('user', 'sentence')
admin.site.register(Bookmark, BookmarkAdmin)

class TopicAdmin(admin.ModelAdmin):
    list_display = ('user', 'language', 'ask_count')
admin.site.register(Topic, TopicAdmin)
