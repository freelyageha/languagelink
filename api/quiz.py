from lang.models import QuizBook#, Quiz
from lang.serializers.quiz import QuizBookSerializer#, QuizSerializer
from django.http import Http404

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


class QuizBookList(APIView):

    def get(self, request, format=None):
        quizbooks = QuizBook.objects.all()
        serializer = QuizBookSerializer(quizbooks, many=True)
        return Response(serializer.data)


class QuizBookDetail(APIView):

    def get_object(self, pk):
        try:
            return QuizBook.objects.get(pk=pk)
        except QuizBook.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        quizbook = self.get_object(pk)
        serializer = QuizBookSerializer(quizbook)
        return Response(serializer.data)

