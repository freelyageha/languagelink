from django.conf.urls import patterns, url
from django.conf import settings

from lang.api.quiz import (
        QuizBookList, QuizBookDetail,
        )

urlpatterns = patterns('lang.views.quiz',
    url(r'^quizbook/$', QuizBookList.as_view()),
    url(r'^quizbook/(?P<pk>[0-9]+)/$', QuizBookDetail.as_view()),
)

