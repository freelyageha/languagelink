from django.conf.urls import *
from django.conf import settings
from django.contrib.auth.decorators import login_required

from lang.views.user import (ProfileRedirect, ProfileUpdate, ProfileDetail,
        ProfileSetting, SentConfirmEmail, SendConfirmEmail, EmailLogin,
        SNSUpdate, SNSCreate, OfftheNewbieFlag)

urlpatterns = patterns('',
    url(r'^login/$', 'django.contrib.auth.views.login',
        name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {
        'next_page': '/',
        }, name='logout'),
    url(r'^offthenewbieflag/$', OfftheNewbieFlag.as_view(),
        name='offthenewbieflag',
        ),
    url(r'^send_confirm_email/$', SendConfirmEmail.as_view(),
        name='send_confirm_email',
        ),
    url(r'^sent_confirm_email/$', SentConfirmEmail.as_view(),
        name='sent_confirm_email',
        ),
    url(r'^confirm_email/(\w+)/$',
        'emailconfirmation.views.confirm_email',
        ),
    #url(r'^email_login/$', EmailLogin.as_view(),
    #    name='email_login'),


) + patterns('underline.views.user',
    url(r'^redirect/$', ProfileRedirect.as_view(),
        name='profile_redirect'),
    url(r'^(?P<pk>[-\.\w]+)/setting/$',
        ProfileSetting.as_view(), name='profile_setting'),
    url(r'^(?P<pk>[-\.\w]+)/update/$',
        ProfileUpdate.as_view(), name='profile_update'),
    url(r'^(?P<pk>[-\.\w]+)/sns_update/$',
        SNSUpdate.as_view(), name='sns_update'),
    url(r'^(?P<pk>[-\.\w]+)/sns_create/$',
        SNSCreate.as_view(), name='sns_create'),
    url(r'^(?P<pk>[-\.\w]+)/detail/$',
        ProfileDetail.as_view(), name='profile_detail'),
)
