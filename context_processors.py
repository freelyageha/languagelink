from django.conf import settings
from lang.models import Language

def get_contexts(request):
    return {
            "FACEBOOK_APP_ID": settings.FACEBOOK_APP_ID,
            "languages": Language.objects.all()
            }
