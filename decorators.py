from django.contrib.auth.decorators import user_passes_test, login_required
from django.core.urlresolvers import reverse

_active_required = user_passes_test(
        lambda u: not u.is_authenticated() or not u.is_new,
        login_url='/accounts/redirect/')

def active_required(view_func):
    decorated_view_func = _active_required(view_func)
    return decorated_view_func


_newbie_required = user_passes_test(
        lambda u: u.is_new,
        login_url='/accounts/redirect/')

def newbie_required(view_func):
    decorated_view_func = _newbie_required(view_func)
    return decorated_view_func


_email_verify_required = user_passes_test(
        lambda u: not u.is_authenticated() or (u.is_authenticated() and u.is_confirmed_email()),
        login_url='/accounts/sent_confirm_email/')

def email_verify_required(view_func):
    decorated_view_func = _email_verify_required(view_func)
    return decorated_view_func


_staff_required = user_passes_test(
        lambda u: u.is_authenticated() and u.is_staff,
        login_url='/accounts/redirect/')

def staff_required(view_func):
    decorated_view_func = _staff_required(view_func)
    return decorated_view_func
