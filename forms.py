# -*- coding: utf-8 -*-

from django.utils.translation import ugettext_lazy
from django import forms

from lang.models import Ask

class AskForm(forms.ModelForm):
    title = forms.CharField()
    context = forms.CharField(widget=forms.Textarea)

    class Meta:
        model = Ask
        exclude = (
                'user',
                'ip_addr',
                'original_context',
                'language',
                )

    def __init__(self, user, *args, **kwargs):
        super(AskForm, self).__init__(*args, **kwargs)
        self.fields.keyOrder = ['title', 'context', 'topic']

        language = user.get_learning_language().get_name_display().capitalize()

        for field_name in self.fields:
            field = self.fields.get(field_name)
            field.widget.attrs['class'] = 'input-block-level'

        title = self.fields.get('title')
        title.label = ""
        title.required = False
        title.widget.attrs['placeholder'] = ugettext_lazy('Title')

        context = self.fields.get('context')
        context.label = ""
        context.widget.attrs['rows'] = '7'
        context.widget.attrs['placeholder'] =  \
                ugettext_lazy("Write about events, experiences, likes, dislikes, hobbies, or anything else that's on your mind!")

        topic = self.fields.get('topic')
        topic.label = ""
        topic.required = False
        topic.widget = forms.HiddenInput()
