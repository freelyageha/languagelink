from django.template import Context
from django.template.loader import get_template
from django.core.mail import send_mail, EmailMessage
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.core.urlresolvers import reverse

class LingRingMailman(object):
    sender = settings.DEFAULT_FROM_EMAIL

    def send(self, title, template_name, language, objects_dict, sender, recipient_list):
        sender = sender if sender else self.sender
        t = get_template('mail/%s/%s' % (language, template_name))
        message = t.render(Context(objects_dict))
        msg = EmailMessage(title, message, sender, recipient_list)
        msg.content_subtype = 'html'
        msg.send()

    def correction(self, screen_name, sentences, ask):
        if not ask.user.subscribe_correction:
            return True

        self.send(
                title='%s gave corrections on your post in "Ling Ring"' % screen_name,
                template_name='correction.html',
                language='english',
                objects_dict={
                    'post_url': reverse('ask_view', args=(ask.id, )),
                    'sentences': sentences,
                    },
                sender="LingRing <admin@lingring.net>",
                recipient_list=[ask.user.email],
                )
 
    def reply(self, screen_name, reply, ask, receivers):
        recipient_list = []
        for receiver in receivers:
            if not receiver.subscribe_reply:
                continue
            recipient_list.append(receiver.email)

        self.send(
                title='%s replied to you in "Ling Ring"' % screen_name,
                template_name='reply.html',
                language='english',
                objects_dict={
                    'post_url': reverse('ask_view', args=(ask.id, )),
                    'reply': reply,
                    },
                sender="LingRing <admin@lingring.net>",
                recipient_list=recipient_list,
                )

    def ring_created(self, student, tutor):
        if not tutor.subscribe_ring_request:
            return True

        self.send(
                title='Request for Ring creation, %s wants you to be %s\'s tutor in "Ling Ring"' % (student, student),
                template_name='ring_created.html',
                language='english',
                objects_dict={
                    'ring_url': reverse('ring_list'),
                    'student': student,
                    },
                sender="LingRing <admin@lingring.net>",
                recipient_list=[tutor.email],
                )
 
    def ring_accepted(self, tutor, student):
        if not student.subscribe_ring_request:
            return True

        self.send(
                title='Accepted for Ring, %s is your tutor in "Ling Ring"' % (tutor),
                template_name='ring_accepted.html',
                language='english',
                objects_dict={
                    'ring_url': reverse('ring_list'),
                    'tutor': tutor,
                    },
                sender="LingRing <admin@lingring.net>",
                recipient_list=[student.email],
                )
 
    def posted_by_student(self, ask):
        writer = ask.user
        tutors = writer.my_tutors()
        recipient_list = []

        for ring in tutors:
            if not ring.tutor.subscribe_student_post:
                continue

            tutor_language = ring.tutor.get_native_language()
            writer_language = writer.get_learning_language()
            if tutor_language == writer_language:
                recipient_list.append(ring.tutor.email)

        if recipient_list:
            self.send(
                    title='Your student, %s, wrote a new post.' % writer,
                    template_name='posted_by_student.html',
                    language='english',
                    objects_dict={
                        'post_url': reverse('ask_view', args=(ask.id, )),
                        'student': writer,
                        },
                    sender="LingRing <admin@lingring.net>",
                    recipient_list=recipient_list,
                    )

    def contact(self, form):
        sender = form.cleaned_data['name']
        message = form.cleaned_data['message']

        send_mail(
                '%s gave contact message on "Ling Ring"' % sender,
                message,
                sender,
                ['jaehaster@gmail.com'],
                )


    def follow_created(self, follower, follow):
        if not follow.subscribe_ring_request:
            return True

        self.send(
                title='%s marks you as an interesting students!' %(follower,),
                template_name='ring_created.html',
                language='english',
                objects_dict={
                    'ring_url': reverse('ring_list'),
                    'follower': follower,
                    },
                sender="LingRing <admin@lingring.net>",
                recipient_list=[follow.email],
                )
