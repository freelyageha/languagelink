from django.utils import translation
from localeurl import utils

LANG_CODE = ['ko', 'en']#, 'zh-cn', 'cn', 'es', 'ja',

class LocaleMiddleware(object):

    def process_request(self, request):
        if not request.META.has_key('HTTP_ACCEPT_LANGUAGE') or not request.META['HTTP_ACCEPT_LANGUAGE']:
            request.META['HTTP_ACCEPT_LANGUAGE'] = 'en-US;q=0.6,en;q=0.4'
        locale = utils.supported_language(
            request.META['HTTP_ACCEPT_LANGUAGE'].split(',')[0])
        if not locale in LANG_CODE:
            locale = 'en'
        translation.activate(locale)
        request.LANGUAGE_CODE = translation.get_language()
