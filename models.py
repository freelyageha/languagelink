import json, math
from operator import itemgetter
from datetime import date, datetime, timedelta
from isoweek import Week

from django.db import models
from django.db.models import Q, F, Max, Count, Sum, Avg
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from django.core.validators import MinLengthValidator
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User, Group
from django.contrib.sites.models import Site
from django.utils.safestring import mark_safe
from django.utils.html import escape
from django.utils.timezone import utc
from django.utils.translation import ugettext_lazy 

from easy_thumbnails.fields import ThumbnailerImageField

from lang import utils
from lang.choices import (LANGUAGE_CHOICE, LOCATION_CHOICES, FEEDBACK_CHOICE,
        NOTIFICATION_CHOICE, FRIEND_STATUS_CHOICE, SENTENCE_TYPE_CHOICE,
        WHERE_POINT_CHOICE, POINT_TYPE_CHOICE, POINT_VALUES, GRADE_VALUES,
        RANK_TYPE_CHOICE
        )
from lang.mail import LingRingMailman


class Language(models.Model):
    name = models.CharField(
            verbose_name=ugettext_lazy("Language"),
            max_length=50,
            choices=LANGUAGE_CHOICE,
            db_index=True,
            )

    def __unicode__(self):
        return "%s" % self.get_name_display()

    def native_users(self):
        return self.native_language.count()

    def learning_users(self):
        return self.learning_language.count()

    def posts(self):
        return self.ask_set.count()

    def this_week_best_tutors(self):
        today = date.today()
        previous_week_year = today.year if today.month != 1 else today.year - 1
        previous_week = today.isocalendar()[1] - 1

        w = Week(previous_week_year, previous_week)
        language = self

        try:
            rank_period = RankPeriod.objects.get(
                    language=language,
                    week=previous_week, year=previous_week_year
                    )
            ranks = Rank.objects.filter(period=rank_period).distinct()
        except ObjectDoesNotExist:
            ranks = []
        return ranks

    def this_week_best_tutors_to_top_five(self):
        best_tutors = self.this_week_best_tutors()
        if best_tutors:
            return best_tutors[:3]
        return None


class SNS(models.Model):
    kakao = models.CharField('KakaoTalk ID',
            max_length=50, null=True, blank=True, )
    line = models.CharField('Line ID',
            max_length=50, null=True, blank=True, )
    skype = models.CharField('Skype ID',
            max_length=50, null=True, blank=True, )
    message = models.TextField(null=True, blank=True, )


class UserProfile(User):
    is_new = models.BooleanField(
            default=True,
            )
    screen_name = models.CharField(
            verbose_name=ugettext_lazy("ID"),
            max_length = 40,
            db_index=True,
            )
    profile_photo = ThumbnailerImageField(
            upload_to='user', blank=True)
    native_language = models.ManyToManyField(Language,
            verbose_name=ugettext_lazy("Native Language"),
            related_name="native_language",
            )
    learning_language = models.ManyToManyField(Language,
            verbose_name=ugettext_lazy("Learning Language(Language I want to learn)"),
            related_name="learning_language",
            )
    location = models.CharField(
            verbose_name=ugettext_lazy("Country (Option)"),
            choices=LOCATION_CHOICES,
            max_length=20,
            null=True, blank=True,
            help_text = '',
            )
    description = models.TextField(
            verbose_name=ugettext_lazy("Profile Message (Option)"),
            null=True, blank=True,
            help_text = '',
            )
    is_newbie = models.BooleanField(
            default=True,
            )
    subscribe_correction = models.BooleanField(
            ugettext_lazy('Correction'), default=True)
    subscribe_reply = models.BooleanField(
            ugettext_lazy('Reply'), default=True)
    subscribe_ring_request = models.BooleanField(
            ugettext_lazy('Ring Request'), default=True)
    subscribe_student_post = models.BooleanField(
            ugettext_lazy('Student Post'), default=True)

    sns = models.OneToOneField(SNS,
            null=True, blank=True, )

    def __str__(self):  
        return "%s" % self.screen_name

    def __unicode__(self):  
        return "%s" % self.screen_name

    def profile(self):
        description = self.description if self.description else ""
        return  description[:30] if len(description) > 30 else description

    def _get_profile_photo(self, size='normal'):
        social_auth = self.social_auth.all()[0]
        if social_auth.provider == 'facebook':
            querystr = ''
            if size == 'normal':
                querystr = '&width=80&height=80'
            elif size == 'small':
                querystr = '&width=50&height=50'
            return "https://graph.facebook.com/%s/picture?type=%s%s" % (social_auth.uid, size, querystr)

    def get_profile_photo(self):
        return self._get_profile_photo()

    def get_small_profile_photo(self):
        return self._get_profile_photo('small')

    def get_absolute_url(self):
        return reverse("ask_post_list", args=(self.screen_name, ))

    def get_primary_email(self):
        try:
            return self.emailaddress_set.get(primary=True)
        except:
            return None

    def get_current_email(self, email):
        try:
            return self.emailaddress_set.get(email=email)
        except:
            return None

    def is_confirmed_email(self):
        email = self.email
        current_email = self.get_current_email(email)
        if current_email:
            return current_email.verified
        return None

    def get_native_language(self):
        return self.native_language.all()[0] \
                if self.native_language.exists() else None

    def get_learning_language(self):
        return self.learning_language.all()[0] \
                if self.learning_language.exists() else None

    def get_post_count(self):
        return self.ask_set.filter(language=self.get_learning_language()).count()

    def get_corrections(self):
        return self.correction_set.filter(
                ask__language=self.get_native_language())

    def get_correction_count(self):
        return self.get_corrections().count()

    def get_correction_notifications(self):
        notifications = Notification.objects.filter(
                receiver=self, is_new=True, type='correction',
                )
        return notifications

    def get_reply_notifications(self):
        notifications = Notification.objects.filter(
                receiver=self, is_new=True, type='reply',
                )
        return notifications

    def get_my_notifications(self):
        notifications = Notification.objects.filter(
                receiver=self, post__user=self,
                is_new=True, type__in=['reply', 'correction'],
                )
        return notifications

    def get_other_post_notifications(self):
        notifications = Notification.objects.filter(
                Q(receiver=self, is_new=True, type='reply'),
                ~Q(post__user=self)
                )
        return notifications

    def get_talk_notifications(self):
        notifications = self.received_notification.filter(
                is_new=True, type='talk',
                )
        return notifications

    def get_student_notifications(self):
        notifications = self.received_notification.filter(
                is_new=True, type='ring', message=None,
                )
        return notifications

    def get_tutor_notifications(self):
        notifications = self.received_notification.filter(
                Q(is_new=True, type='ring'), ~Q(message=None),
                )
        return notifications

    def recommend_tutors(self):
        _best_tutors = list(self.get_learning_language().this_week_best_tutors()[:5].values_list('user__id', flat=True))

        tutor_lists = self.my_tutors().values_list('tutor__id', flat=True)
        corrections = Correction.objects.filter(ask__in=self.ask_set.all())
        correctors = UserProfile.objects.filter(
                Q(correction__in=corrections, native_language=self.get_learning_language()) | 
                (Q(id__in=_best_tutors) & ~Q(screen_name='') & ~Q(screen_name=None))
                )
        recommend_tutors = correctors.filter(~Q(id__in=tutor_lists)
                ).annotate(c_count=Count('id', distinct=True)
                ).order_by('?')
        if recommend_tutors.count() > 6:
            return recommend_tutors[:6]
        return recommend_tutors

    def get_ring_notifications(self):
        notifications = self.received_notification.filter(
                is_new=True, type__in=['ring', 'talk'],
                )
        return notifications

    def get_last_post(self):
        if self.ask_set.all().exists():
            return self.ask_set.all()[0]
        return ''

    def get_last_correction(self):
        corrections = self.get_corrections()
        if corrections.exists():
            correction = corrections[0]
            title = correction
            time = utils.get_elapsed_time(correction.ctime)
            return '"%s" in %s' % (title, time)
        return ''

    def facebook_uid(self):
        try:
            return self.social_auth.get(provider='facebook').uid
        except MultipleObjectsReturned:
            return self.social_auth.filter(provider='facebook')[0].uid

    def my_requested_tutors(self):
        return self.student.filter(status='requested')

    def my_received_tutors(self):
        return self.tutor.filter(status='requested')

    def my_tutors(self):
        return self.student.filter(status='accepted')

    def my_students(self):
        return self.tutor.filter(status='accepted')

    def my_follower(self):
        return self.student.filter(follow=True)

    def my_following(self):
        return self.tutor.filter(follow=True)

    def my_chatrooms(self):
        return self.chatroom.all()

    def max_ring(self):
        grade = self.point._grade()
        """
        Jeahaster max ring up temporary
        """
        if self.screen_name == 'jaehaster':
            return 100
        return GRADE_VALUES[grade]['ring']

    def can_request_tutor(self):
        tutors = self.my_tutors().count()
        request_tutors = self.my_requested_tutors().count()
        ring_cutoff = self.max_ring()

        if ring_cutoff <= (tutors + request_tutors):
            return False
        return True

    def gender(self):
        extra_data = self.social_auth.get(provider='facebook').extra_data
        return extra_data['gender'] if extra_data.has_key('gender') else ""

    def birthday(self):
        extra_data = self.social_auth.get(provider='facebook').extra_data
        return extra_data['birthday'] if extra_data.has_key('birthday') else ""

    def interaction_rate(self):
        received_corrections = Correction.objects.filter(
                ask__in=self.ask_set.all())
        received_corrections = \
                received_corrections.count() if received_corrections else 0
        my_reply = Correction.objects.filter(ask__in=self.ask_set.all()).filter(
                Q(sentence__comment__user=self) | Q(overallcomment__user=self)
                ).distinct().count()

        my_reply = my_reply if my_reply else 0
        if received_corrections == 0:
            return 0
        return 100 * my_reply / received_corrections

    def set_level(self):
        interaction_rate = self.interaction_rate()

        received_corrections = Correction.objects.filter(
                ask__in=self.ask_set.all())
        received_corrections = \
                received_corrections.count() if received_corrections else 0

        if interaction_rate == 0 and not received_corrections:
            group, created = Group.objects.get_or_create(name='N/A')
        elif interaction_rate >= 90:
            group, created = Group.objects.get_or_create(name='Excellent')
        elif interaction_rate >= 80:
            group, created = Group.objects.get_or_create(name='Great')
        elif interaction_rate >= 70:
            group, created = Group.objects.get_or_create(name='Good')
        elif interaction_rate >= 50:
            group, created = Group.objects.get_or_create(name='Normal')
        else:
            group, created = Group.objects.get_or_create(name='Poor')

        self.groups.clear()
        self.groups.add(group)

    def get_level(self):
        if self.groups.exists():
            return self.groups.all()[0]
        else:
            return Group.objects.get(name='N/A')

    def get_last_quiz_score(self):
        quizbooks = self.quizbook_set.filter(~Q(score=None))
        if quizbooks:
            return quizbooks[0].score
        else:
            return ''

    def is_enough_posts(self):
        return self.ask_set.exists()

    def is_enough_corrections(self):
        return Correction.objects.filter(
                Q(ask__in=self.ask_set.all()),
                ~Q(
                    Q(sentence__context=None) |
                    Q(sentence__context='[Perfect]') |
                    Q(sentence__context='[Do-not-understand]')
                    )
                ).exists()

    def is_solved_today_quiz(self):
        today = datetime.today().strftime("%Y-%m-%d")
        try:
            quizbooks = QuizBook.objects.filter(user=self, date__startswith=today)
            if quizbooks:
                quizbook = quizbooks.latest('date')
                if quizbook.score:
                    return True
                return False
            return False
        except:
            return False

    def unread_topics(self):
        topics = Topic.objects.filter(
                Q(language=self.get_learning_language()),
                ~Q(reader__in=[self])
                )
        return topics

    def has_unread_topics(self):
        return self.unread_topics().exists()

    def get_average_score(self):
        asks = self.ask_set.filter(
                    is_published=True,
                    correction__ctime__gt=datetime(2014, 5, 24)
                ).annotate(
                    grammer_score=Avg('correction__rating__grammer'),
                    spelling_score=Avg('correction__rating__spelling'),
                    difficulty_score=Avg('correction__rating__difficulty')
                ).order_by('id')

        score_list = \
                list(asks.values_list('grammer_score', flat=True)) + \
                list(asks.values_list('spelling_score', flat=True)) + \
                list(asks.values_list('difficulty_score', flat=True))
        score_average = reduce(lambda x, y: x + y, score_list) / len(score_list)
        return score_average


class Signable(models.Model):
    user = models.ForeignKey(UserProfile,
            verbose_name="Registrator",
            null=True,
            db_index=True,
            )
    ip_addr = models.IPAddressField(
            null=True, blank=True)
    ctime = models.DateTimeField('Created datetime',
            auto_now_add=True)
    mtime = models.DateTimeField('Modified datetime',
            auto_now=True)

    class Meta:
        abstract = True


class Ring(models.Model):
    tutor = models.ForeignKey(UserProfile,
            verbose_name=ugettext_lazy("Tutor"),
            related_name="tutor",
            )
    student = models.ForeignKey(UserProfile,
            verbose_name=ugettext_lazy("Student"),
            related_name="student",
            )
    message = models.CharField(
            verbose_name=ugettext_lazy("Message"),
            max_length=250,
            null=True, blank=True,
            )
    status = models.CharField(
            verbose_name=ugettext_lazy("Friend status"),
            max_length=20,
            choices=FRIEND_STATUS_CHOICE,
            default='requested',
            )
    follow = models.BooleanField(default=False)
    ctime = models.DateTimeField('Created datetime',
            auto_now_add=True)
    mtime = models.DateTimeField('Modified datetime',
            auto_now=True)

    class Meta:
        ordering = ('-mtime',)
        unique_together = ("tutor", "student")
        index_together = [
                    ["tutor", "student"],
                    ]

    def save(self, *args, **kwargs):
        super(Ring, self).save(*args, **kwargs)
        if self.status == 'requested':
            notification, created = Notification.objects.get_or_create(
                    type='ring', ring=self, receiver=self.tutor)
            notification.sender.add(self.student)
        elif self.status == 'accepted':
            self.notification.update(is_new=False)
            notification, created = Notification.objects.get_or_create(
                    type='ring', ring=self, receiver=self.student)
            notification.sender.add(self.tutor)
            notification.message = "%s accepted to be your tutor with happiness!" % self.tutor
        elif self.status == 'follow':
            notification, created = Notification.objects.get_or_create(
                    type='ring', ring=self, receiver=self.student)
            notification.sender.add(self.student)
            notification.message = "%s marks you as an interesting students!" % self.tutor

        notification.is_new = True
        notification.save()

        try:
            mailman = LingRingMailman()
            if self.status == 'requested':
                mailman.ring_created(self.student, self.tutor)
            elif self.status == 'accepted':
                mailman.ring_accepted(self.tutor, self.student)
            elif self.status == 'follow':
                mailman.follow_created(self.tutor, self.student)
        except:
            pass
        return self

    def elapsed_time(self):
        return utils.get_elapsed_time(self.ctime)

    def summary(self):
        length = 66
        context = self.message
        letters = [l for l in context]

        clength = 0
        cstring = ""
        for letter in letters:
            if clength >= length:
                break
            if ord(letter) <= 128:
                cstring += letter
                clength += 1
            else:
                cstring += letter
                clength += 2
        if len(cstring) < len(context):
            cstring += '...'
        return cstring

    def get_absolute_url(self):
        return reverse('ring_list')


class Tag(models.Model):
    name = models.CharField(
            max_length=50
            )
    user = models.ForeignKey(UserProfile,
            null=True, blank=True,
            )


class Topic(Signable):
    language = models.ForeignKey(Language)
    title = models.CharField(max_length='255')
    reader = models.ManyToManyField(UserProfile,
            related_name='readers', null=True, blank=True, )

    class Meta:
        ordering = ('-id',)

    def __unicode__(self):
        return "(%s) %s" % (self.language.name, self.title,)

    def get_elapsed_time(self):
        return utils.get_elapsed_time(self.ctime)

    def ask_count(self):
        return self.ask_set.count()


class Ask(Signable):

    class Meta:
        ordering = ('-bump_time',)

    context = models.TextField(
            verbose_name=ugettext_lazy("Full Text"),
            validators=[MinLengthValidator(15)],
            )
    original_context = models.TextField(
            verbose_name=ugettext_lazy("Original Full Text"),
            null=True, blank=True,
            )
    language = models.ForeignKey(Language,
            verbose_name=ugettext_lazy('Language'),
            null=True, blank=True,
            db_index=True,
            )
    bump_time = models.DateTimeField('Created datetime',
            auto_now_add=True,
            db_index=True,
            )
    is_published = models.BooleanField(
            default=False
            )
    topic = models.ForeignKey(Topic, null=True, blank=True, )


    def save(self, *args, **kwargs):
        if kwargs.pop('bump_time', True):
            self.bump_time = datetime.utcnow().replace(tzinfo=utc)
        super(Ask, self).save(*args, **kwargs)

    def __str__(self):  
        return "%s" % self.title() if self.title() else self.get_summary(15)

    def __unicode__(self):  
        return "%s" %self.title() if self.title() else self.get_summary(15)

    def title(self):
        if self.sentence_set.filter(type='title').exists():
            return self.sentence_set.get(type='title')
        else:
            return ""

    def sentences(self):
        if self.sentence_set.filter(type='sentence').exists():
            return self.sentence_set.filter(type='sentence')
        else:
            return ""

    def comments(self):
        if self.overallcomment_set.all().exists():
            return self.overallcomment_set.all()
        else:
            return ""

    def get_absolute_url(self):
        return reverse('ask_view', args=(self.id, )) + "?success=true"

    def get_elapsed_time(self):
        return utils.get_elapsed_time(self.ctime)

    def get_summary(self, length=135):
        context = self.context
        letters = [l for l in context]

        clength = 0
        cstring = ""
        for letter in letters:
            if clength >= length:
                break

            if ord(letter) <= 128:
                cstring += letter
                clength += 1
            else:
                cstring += letter
                clength += 2

        if len(cstring) < len(context):
            cstring += '...'
        title = self.title().context if self.title() else ""
        return mark_safe(
            '<span class="title">' + escape(title) + '</span>' + escape(cstring)
            )

    def get_corrections(self):
        return self.correction_set.all()

    def get_correctors(self):
        return UserProfile.objects.filter(correction__ask=self)

    def get_replies(self):
        corrections = Sentence.objects.filter(
                parent__in=self.sentence_set.all())
        comments = Comment.objects.filter(
                ~Q(context=""), Q(parent__in=corrections))
        overall_comments = OverallComment.objects.filter(
                correction__in=self.correction_set.all()
                )
        return comments.count() + overall_comments.count()

    def has_correct_notifications(self):
        return self.notification.filter(
                receiver=self.user,
                is_new=True, type__in=['reply', 'correction']).exists()

    def reply_notifications(self):
        return self.notification.filter(
                is_new=True, type="reply")

    def has_correction(self):
        return self.notification.all().exists()

    def is_passed_2_days(self):
        if (datetime.utcnow().replace(tzinfo=utc) - self.ctime).days >= 2:
            return True
        return False

    def is_bumpedup(self):
        if self.pointlog_set.exists():
            return True
        return False

    def is_possible_bumpup(self):
        if self.is_passed_2_days() and not self.correction_set.exists() and not self.is_bumpedup():
            return True
        return False

    def average_score(self):
        ask = Ask.objects.filter(id=self.id).annotate(
                grammer_score=Avg('correction__rating__grammer'),
                spelling_score=Avg('correction__rating__spelling'),
                diffculty_score=Avg('correction__rating__difficulty')
                )[0]
        return int(math.ceil((ask.grammer_score + ask.spelling_score + ask.diffculty_score) / 3.0))

class Rating(models.Model):
    grammer = models.SmallIntegerField(
            null=True, blank=True, )
    spelling = models.SmallIntegerField(
            null=True, blank=True, )
    difficulty = models.SmallIntegerField(
            null=True, blank=True, )
    comment = models.TextField(null=True, blank=True, )

    def overall(self):
        if not self.grammer and not self.spelling and not self.difficulty:
            return None

        return math.ceil((
                (self.grammer if self.grammer else 0) + 
                (self.spelling if self.spelling else 0) + 
                (self.difficulty if self.difficulty else 0)
            ) / 3.0)


class Correction(Signable):
    ask = models.ForeignKey(Ask,
            null=True, blank=True,
            db_index=True,
            )
    score = models.IntegerField(null=True, blank=True,)
    rating = models.OneToOneField(Rating, null=True, blank=True)

    class Meta:
        ordering = ('-id',)

    def title(self):
        w_titles = Sentence.objects.select_related('').filter(
                type='title')
        titles = w_titles.filter(Q(correction=self) | 
                (Q(ask=self.ask) & ~Q(child__correction=self)))
        if titles:
            return titles[0]
        else:
            return None

    def sentences(self):
        w_sentences = Sentence.objects.select_related('').filter(
                type='sentence')
        sentences = w_sentences.filter(Q(correction=self) | 
                (Q(ask=self.ask) & ~Q(child__correction=self)))
        return sentences.extra(
                select={"oid": "COALESCE(parent_id, id)"},
                order_by=["oid"])

    def elapsed_time(self):
        return utils.get_elapsed_time(self.ctime)

    def represent_comment(self):
        comments = self.overallcomment_set.filter(fb_flag=False)
        if comments.exists():
            return comments[0]
        return None


class OverallComment(Signable):
    correction = models.ForeignKey(Correction,
            null=True, blank=True,
            db_index=True,
            )
    fb_flag = models.BooleanField(default=False)
    context = models.TextField(
            verbose_name=ugettext_lazy("Comment"),
            )
    class Meta:
        ordering = ('id',)


class Sentence(Signable):

    class Meta:
        ordering = ('id',)

    ask = models.ForeignKey(Ask,
            null=True, blank=True,
            db_index=True,
            )
    correction = models.ForeignKey(Correction,
            null=True, blank=True,
            db_index=True,
            )
    type = models.CharField(
            choices=SENTENCE_TYPE_CHOICE,
            default="sentence",
            max_length=15,
            )
    parent = models.ForeignKey("self",
            related_name="child",
            null=True, blank=True,
            db_index=True,
            )
    context = models.TextField(
            verbose_name=ugettext_lazy("Context"),
            )
    native_context = models.TextField(
            verbose_name=ugettext_lazy("Native context"),
            null=True, blank=True,
            )
    html_context = models.TextField(
            verbose_name=ugettext_lazy("Html Context"),
            null=True, blank=True,
            )
    tags = models.ManyToManyField(Tag,
            null=True, blank=True,
            )

    def __str__(self):
        return "%s" % self.context

    def __unicode__(self):
        return "%s" % self.context

    def children(self):
        return self.child.all()

    def replies(self):
        return self.comment_set.filter(~Q(context=""))


class Comment(Signable):
    parent = models.ForeignKey(Sentence,
            verbose_name=ugettext_lazy("Parent Sentence"),
            db_index=True,
            )
    context = models.CharField(
            verbose_name=ugettext_lazy("Comment"),
            max_length=500,
            )

    class Meta:
        ordering = ('id',)


class QuizBookManager():
    pass

class QuizBook(Signable):
    date = models.SlugField()
    score = models.PositiveSmallIntegerField(
            null=True, blank=True,
            )
    objects = QuizBookManager()

    class Meta:
        ordering = ('-date',)

    def __unicode__(self):
        return self.date

    def get_date(self):
        split_date = self.date.split('-')
        return "-".join(split_date[:3])

    def get_try_count(self):
        split_date = self.date.split('-')
        if len(split_date) > 3:
            return " - %s" % split_date[3]
        return ""

    def get_quiz(self, quiz_id):
        return self.quiz_set.get(id=quiz_id)

    def get_score(self):
        quizzes = self.quiz_set.all()
        answers = quizzes.filter(answer=True)
        return 100 * answers.count() / quizzes.count()

    def get_quiz_of_count(self):
        quizzes = self.quiz_set.all().count()
        answers = self.quiz_set.filter(answer=True).count()
        return '%d of %d' % (answers, quizzes)
 
    def get_quiz_complete_message(self):
        score = self.get_score()
        if score == 100:
            return ugettext_lazy('Perfect!')
        elif score >= 90:
            return ugettext_lazy('Great!')
        elif score >= 70:
            return ugettext_lazy('Nice!')
        else:
            return ugettext_lazy('Study more!')


def create_quizzes(instance, created, raw, **kwargs):
    if not created or raw:
        return
    book = instance
    user = book.user
    sentences = Sentence.objects.filter(
            Q(user=user),
            ~Q(questions__feedback__in=['bad']),
            ~Q(child=None)
        ).annotate(child_count=Count('child', distinct=True)
        ).annotate(tag_count=Count('tags', distinct=True)
        ).filter(~Q(child_count=F('tag_count'))).order_by('?')[:10]

    invite_flag = False
    if user.invite_set.exists():
        invite_flag = True

    created_question_count = 0
    for sentence in sentences:
        if created_question_count == 5:
            break
        correct_answers = sentence.child.filter(
                ~Q(context__startswith='[Perfect]'),
                ~Q(context__startswith='[Do-not-understand]')
                )
        if not correct_answers.exists():
            continue
        correct_answer = correct_answers.order_by('?')[0]
        Quiz.objects.create(quizbook=book,
                question=sentence, correct_answer=correct_answer)
        created_question_count += 1

    if invite_flag:
        sentences = Sentence.objects.filter(
                ~Q(user=user),
                ~Q(questions__feedback__in=['bad']),
                ~Q(child=None),
                Q(ask__language=user.get_learning_language)
            ).annotate(child_count=Count('child', distinct=True)
            ).annotate(tag_count=Count('tags', distinct=True)
            ).filter(~Q(child_count=F('tag_count'))).order_by('?')[:10]

        created_question_count = 0
        for sentence in sentences:
            if created_question_count == 5:
                break
            correct_answers = sentence.child.filter(
                    ~Q(context__startswith='[Perfect]'),
                    ~Q(context__startswith='[Do-not-understand]')
                    )
            if not correct_answers.exists():
                continue
            correct_answer = correct_answers.order_by('?')[0]
            Quiz.objects.create(quizbook=book,
                    question=sentence, correct_answer=correct_answer)
            created_question_count += 1

models.signals.post_save.connect(create_quizzes, sender=QuizBook)


class Quiz(models.Model):

    class Meta:
        ordering = ('-id',)

    quizbook = models.ForeignKey(QuizBook,
            verbose_name=ugettext_lazy("quizzes"),
            db_index=True,
            )
    question = models.ForeignKey(Sentence,
            verbose_name=ugettext_lazy("Question"),
            related_name='questions',
            db_index=True,
            )
    correct_answer = models.ForeignKey(Sentence,
            verbose_name=ugettext_lazy("Correct answer"),
            related_name='correct_answers',
            )
    answer = models.BooleanField(default=False)
    hint_open_count = models.PositiveSmallIntegerField(
            default=0,
            )
    feedback = models.CharField(
            max_length=20, choices=FEEDBACK_CHOICE,
            null=True, blank=True,
            )


class ChatRoom(models.Model):

    member = models.ManyToManyField(UserProfile,
            related_name="chatroom",
            )
    mtime = models.DateTimeField('Modified datetime',
            auto_now=True)

    class Meta:
        ordering = ('-id',)

    def members(self):
        return self.member.all()

    def get_latest_message(self):
        return self.messages.latest('ctime')

    def get_message_count(self):
        return self.messages.count()

    def notifications(self):
        return self.notification.filter(is_new=True)


class Message(Signable):

    class Meta:
        ordering = ('-ctime', )

    room = models.ForeignKey(ChatRoom,
            related_name="messages",
            )
    context = models.TextField(
            verbose_name=ugettext_lazy("Message"),
            )
    read_count = models.IntegerField(
            default=1,
            )

    def get_absolute_url(self):
        return reverse(
                'open_chatroom', args=(self.room.id, ))

    def get_elapsed_time(self):
        return utils.get_elapsed_time(self.ctime)


class Notification(models.Model):
    post = models.ForeignKey(Ask,
            verbose_name=ugettext_lazy("Post"),
            related_name='notification',
            null=True, blank=True,
            )
    talk = models.ForeignKey(ChatRoom,
            verbose_name=ugettext_lazy("Talk"),
            related_name='notification',
            null=True, blank=True,
            )
    ring = models.ForeignKey(Ring,
            verbose_name=ugettext_lazy("Ring"),
            related_name='notification',
            null=True, blank=True,
            )
    type = models.CharField(
            verbose_name=ugettext_lazy("Notification Type"),
            choices=NOTIFICATION_CHOICE,
            max_length=30,
            )
    is_new = models.BooleanField(
            default=True,
            )
    sender = models.ManyToManyField(UserProfile,
            verbose_name=ugettext_lazy("Sender"),
            related_name='sended_notification',
            null=True, blank=True,
            )
    receiver = models.ForeignKey(UserProfile,
            verbose_name=ugettext_lazy("Receiver"),
            related_name='received_notification',
            db_index=True,
            )
    ctime = models.DateTimeField('Created datetime',
            auto_now_add=True)
    mtime = models.DateTimeField('Modified datetime',
            auto_now=True)
    message = models.CharField(
            verbose_name=ugettext_lazy("Message"),
            max_length=250,
            null=True, blank=True,
            )

    def on(self):
        self.is_new = True
        self.save()

    def off(self):
        if self.is_new:
            self.is_new = False
            self.save()


class ReleaseNote(Signable):
    title = models.CharField(
            max_length=250,
            )
    context = models.TextField(
            null=True, blank=True,
            )
    html_context = models.TextField(
            null=True, blank=True,
            )


class Invite(Signable):
    to = models.CharField(max_length=30, )
    is_visited = models.BooleanField(default=False)
    is_joined = models.BooleanField(default=False)


class Point(models.Model):
    user = models.OneToOneField(UserProfile)
    all_point = models.IntegerField(
            default=0
            )
    used_point = models.IntegerField(
            default=0
            )

    def __unicode__(self):
        return "%s" % (self.all_point - self.used_point)
        #return "%s (accure: %s)" % ((self.all_point - self.used_point), self.all_point)

    def acquire_point(self, where, ask=None, **kwargs):
        point = POINT_VALUES[where]
        if kwargs and kwargs.has_key('sentences'):
            point += POINT_VALUES['correction_sentence'] * kwargs['sentences']

        log, created = PointLog.objects.get_or_create(
                parent=self, where=where, flag='get', ask=ask)

        is_grade_up = ''
        if created:
            log.point=point
            log.save()

            sorted_grade = sorted(GRADE_VALUES.items())
            for index, grade in sorted_grade:
                cutoff = grade['cutoff']
                cp = self.all_point
                if cp < cutoff and cp + point >= cutoff:
                    is_grade_up = grade['name']
                    break

            self.all_point += point
            self.save()
            return log, is_grade_up
        return None, is_grade_up

    def lose_point(self, where, ask=None, quizbook=None):
        point = POINT_VALUES[where]
        log, created = PointLog.objects.get_or_create(
            parent=self, point=point, where=where, flag='used',
            ask=ask, quizbook=quizbook)

        if created:
            self.used_point += point
            self.save()
            return log
        return None

    def current_point(self):
        return (self.all_point - self.used_point)

    def last_get_point(self):
        return self.pointlog_set.filter(flag='get').order_by('-id')[0]

    def have_point_for_bumpup(self):
        if self.current_point() >= 50:
            return True
        return False

    def _grade(self):
        ap = self.all_point
        sorted_grade = sorted(GRADE_VALUES.items())
        for key, grade in sorted_grade:
            if ap < grade['cutoff']:
                return key
        return sorted_grade.pop()[0]

    def grade(self):
        grade_index = self._grade()
        return GRADE_VALUES[grade_index]['name']

    def need_point(self):
        ap = self.all_point
        grade_index = self._grade()
        return (GRADE_VALUES[grade_index]['cutoff'] - ap)

    def next_grade(self):
        ap = self.all_point
        grade_index = self._grade()
        last_grade_index, last_grade = sorted(GRADE_VALUES.items()).pop()
        if last_grade_index == grade_index:
            return None
        return GRADE_VALUES[str(int(grade_index) + 1)]['name']


class PointLog(models.Model):

    class Meta:
        ordering = ('-id',)

    parent = models.ForeignKey(Point)
    point = models.IntegerField(null=True, blank=True)
    where = models.CharField(max_length='20', choices=WHERE_POINT_CHOICE, )
    flag = models.CharField(max_length='10', choices=POINT_TYPE_CHOICE, )
    ctime = models.DateTimeField('Created datetime', auto_now_add=True)
    ask = models.ForeignKey(Ask, null=True, blank=True)
    quizbook = models.ForeignKey(QuizBook, null=True, blank=True)

class Event(models.Model):
    active = models.BooleanField()


class Poll(Signable):
    event_id = models.IntegerField()
    context = models.TextField()

    def trans_context(self):
        c = json.loads(self.context)
        s = ""
        for key, value in sorted(c.iteritems()):
            s += "%s: %s, " % (key, value)
        return s


class RankPeriod(models.Model):
    language = models.ForeignKey(Language, null=True, blank=True)
    year = models.IntegerField(null=True, blank=True)
    month = models.IntegerField(null=True, blank=True)
    week = models.IntegerField(null=True, blank=True)


class Rank(models.Model):
    period = models.ForeignKey(RankPeriod)
    user = models.ForeignKey(UserProfile)
    points = models.IntegerField()

    class Meta:
        ordering = ('-points',)


class Bookmark(Signable):
    sentence = models.ForeignKey(Sentence)
    comment = models.CharField(max_length='250', null=True, blank=True)

    class Meta:
        ordering = ('-id',)

    def get_elapsed_time(self):
        return utils.get_elapsed_time(self.ctime)


class UserMeta(models.Model):
    user = models.OneToOneField(UserProfile)
    view_lingring_think = models.BooleanField(default=False)
