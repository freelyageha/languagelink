# -*- coding: utf-8 -*-

from social_auth.backends.facebook import FacebookBackend
from urlparse import parse_qs
from django.conf import settings

import urllib
import os

def get_user_avatar(backend, details, response, social_user, uid,\
                user, *args, **kwargs):

    url = None
    if getattr(backend, 'name', None) == 'facebook':
        url = "http://graph.facebook.com/%s/picture?type=large" % response['id']

    if url:
        filepath = os.path.join(settings.MEDIA_ROOT,
                'user', response['id'] + ".jpg")
        urllib.urlretrieve(url, filepath)
        user.profile_photo = "user/" + response['id'] + ".jpg"
        user.save()
