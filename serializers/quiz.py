from django.forms import widgets

from rest_framework import serializers

from lang.models import QuizBook, Quiz


class QuizBookSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuizBook
        fields = ('id', )
