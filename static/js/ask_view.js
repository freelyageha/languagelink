function post_reply($pdiv, reply_url, reply_photo_url, replier_profile_url) {
    $.ajax({
        url: reply_url,
        dataType: 'json',
        type: 'post',
        data: $pdiv.find('input').serialize(),
        beforeSend: function(xhr) {
          $pdiv.find('.reply').hide();
          $pdiv.find('.sending').show();
        },
        success: function(result) {
            var $comment = $("<div>").addClass("media");
            var $comment_user = $("<a>").attr("href", reply_photo_url).append($("<img>").addClass("media-object media-object-32 pull-left").attr("src", replier_profile_url));
            var $comment_text_wrapper = $("<div>").addClass("media-body");
            var $context_wrapper = $("<div>").addClass("context_wrapper");
            var $comment_text = $("<div>").addClass('text').text(result['comment']);
            var $comment_time = $("<span>")
                    .addClass("muted").text(" " + result['ctime'] + " ");
            var $edit_btn = $("<a>").attr("href", "#")
                    .addClass("UIEditButton").text("수정");
            $context_wrapper.append($comment_text);
            $context_wrapper.append($comment_time);
            $context_wrapper.append($edit_btn);

            var $edit_form = $("<form>").addClass("update_form hide").attr("method", "post")
                    .attr("action", "/comment/" + result['comment_id'] + "/update/");

            var token_value = $pdiv.find("input[name='csrfmiddlewaretoken']").val();
            var $token = $("<input>").attr("name", "csrfmiddlewaretoken")
                    .attr("type", "hidden").val(token_value);
            var $context = $("<input>").attr("type", "text")
                    .attr("name", "context").val(result['comment'])
                    .attr("placeholder", "If you press return, this comment will be deleted");
            var $action_btns = $("<div>").addClass("text-right")
                    .append($("<a>").attr("href", "#").addClass("btn btn-link btn-mini UICancelButton").text('Cancle'))
                    .append($("<button>").addClass("btn btn-link btn-mini UIDoneButton").text('Done'));
            $edit_form.append($token, $context, $action_btns);

            $comment_text_wrapper.append($context_wrapper);
            $comment_text_wrapper.append($edit_form);
            $comment.append($comment_user);
            $comment.append($comment_text_wrapper);
            if ($pdiv.parent().find('.comment_wrapper').length) {
                $pdiv.parent().find('.comment_wrapper').append($comment);
            } else {
                $pdiv.before($('<div class="comment_wrapper">').append($comment));
            }
            $pdiv.find("input.reply_context").val("");
        },
        error: function(result) {
            alert("Something wrong with insert reply");
        },
        complete: function(result) {
            $pdiv.find('.reply').show();
            $pdiv.find('.sending').hide();
            $pdiv.find('.reply .reply_context').focus();
        }
    });
}

function overall_comment($f, profile_url,  photo_url) {
    $.ajax({
        url: $f.attr('action'),
        dataType: 'json',
        type: 'post',
        data: $f.serialize(),
        beforeSend: function(xhr) {
            $f.find('.reply').hide();
            $f.find('.sending').show();
        },
        success: function(result) {
            var $media = $("<div class='media'>")
            var $profile = $("<div class='pull-left'>").append(
                $("<a href='" + profile_url + "'>").append(
                    $("<img class='media-object-32'>").attr("src", photo_url)))
            var $media_body = $("<div class='media-body comment'>")
            var $context_wrapper = $("<div class='context_wrapper'>")
                .append($("<div class='text'>").text(result['comment']))
                .append($("<span class='muted'>").text(result['ctime']))
                .append($("<a href='#' class='UIEditButton'> Edit</a>"));

            var $edit_form = $("<form>").addClass("update_form hide").attr("method", "post")
                    .attr("action", "/overall_comment/" + result['comment_id'] + "/update/");
            var token_value = $f.find("input[name='csrfmiddlewaretoken']").val();
            var $token = $("<input>").attr("name", "csrfmiddlewaretoken")
                    .attr("type", "hidden").val(token_value);
            var $context = $("<input>").attr("type", "text")
                    .attr("name", "context").val(result['comment'])
                    .attr("placeholder", "If you press return, this comment will be deleted");
            $edit_form.append($token).append($context);

            $media.append($profile)
                  .append($media_body.append($context_wrapper).append($edit_form));
            $media.insertBefore($f);
            $f[0].reset();
        },
        error: function(result) {
            $f.find(".alert").fadeIn('slow').delay(1500).fadeOut('slow');
        },
        complete: function(result) {
            $f.find('.reply').show();
            $f.find('.sending').hide();
            $f.find('.reply #context').focus();
        }
    });
}

$(document).ready(function() {
    $(".textareaForCorrection").on("focus", function() {
        if ($(this).hasClass('unlogged_correction')) {
          if (confirm('You need log in before writing. Go to login?'))
            location.href = '/accounts/login';
          $(".textareaForCorrection").blur();
        } else {
          name = $(this).attr('name');
          comment_id = name.replace(/^s_/, "c_");
          $("#" + comment_id).show();
        }
    });

    $(".tags").on("click", function(event) {
        var $wrapper = $(this).parents().closest('.sentence');
        var correction = $($wrapper.find('textarea')[0]);
        var text = ""

        if ($(this).hasClass('correct')) {
            $wrapper.find('.interaction_btn_wrapper .tags').show().removeClass('hide');
            $wrapper.find('form').show().removeClass('hide');
            $(this).parent().hide();
        }
        else if ($(this).hasClass('perfect')) {
            text = "[Perfect]";
        }
        else if ($(this).hasClass('do-not-understand')) {
            text = "[Do-not-understand]";
        }
        else if ($(this).hasClass('copy-to-inputbox')) {
            text = $wrapper.find('.original-sentence .os').text();
        }
        correction.val(text).focus();
        return false;
    });

    $("button.correction_cancel").on("click", function() {
      var $wrapper = $(this).parents().closest('.sentence-wrapper');
      var $tags = $wrapper.find('.interaction_btn_wrapper')
      var $correct_btn = $wrapper.find('.correct_btn_wrapper')
      $wrapper.find('form').hide()[0].reset();
      $tags.find('.tags').hide();
      $correct_btn.show();
    });

    $('.rating').raty({
        path      : "/static/image/rating",
        numberMax : 5,
        number    : 100,
        scoreName: function() { return $(this).data('name'); }
      });
});


$(document).on("click", ".UIEditButton", function() {
    var $wrapper = $(this).parents().closest('.media-body');
    $wrapper.find('.context_wrapper').hide();
    $wrapper.find('.update_form').show().removeClass('hide');
    $($wrapper.find('.update_form input[type="text"]')[0]).focus();
    return false;
});

$(document).on("click", ".UICancelButton", function() {
    var $wrapper = $(this).parents().closest('.media-body');
    $wrapper.find('.update_form').hide();
    $wrapper.find('.context_wrapper').show();
    return false;
});

$(document).on("submit", ".update_form", function() {
    var $f = $(this);
    $.ajax({
        url: $f.attr('action'),
        dataType: 'json',
        type: 'post',
        data: $f.serialize(),
        beforeSend: function(xhr) {
            $f.hide();
            $f.parent().find('.sending').show();
        },
        success: function(result) {
            if (result.status == 'success') {
                if (result['deleted']) {
                  $f.parent().parent().remove();
                  return;
                }

                $f.find('input[type="text"]').val(result.value);
                $f.parent().find('.sending').hide();
                $f.parent().find('.error').hide();
                var $cc = $f.parent().find('.context_wrapper');
                $cc.find('.text').html(result.comment);
                $cc.show();
            } else {
                $f.show();
                $f.find('input[type="text"]').focus();
                $f.parent().find('.error').show().removeClass('hide');
                $f.parent().find('.sending').hide();
            }
        },
        error: function(result) {
          alert('Raised error, please contact us administrator');
        }
    });
    return false;
});


$(document).on("submit", ".bookmark-form", function(event) {
  var $f = $(this);

  $.post($f.attr('action'), $f.serialize())
    .done(function(data) {
      if (data['bookmarked']) {
        var $icon = $f.find('i.fa').addClass('fa-bookmark')
        $icon.removeClass('fa-bookmark-o');
        $icon.parent().find('span').text(' Noted');
      }
      else {
        var $icon = $f.find('i.fa').addClass('fa-bookmark-o')
        $icon.removeClass('fa-bookmark');
        $icon.parent().find('span').text(' Keep to my note');
      }
    })
    .fail(function() {
      alert('Raised error, please try again or contact us administrator');
    });
  return false;
});

$(document).on("click", ".see-detail-correction", function() {
  var $btn = $(this)
  if ($btn.hasClass('open')) {
    $(this).parent().parent().find('del').hide();
    $(this).removeClass('open');
    $(this).text('See detail');
  } else  {
    $(this).parent().parent().find('del').show();
    $(this).addClass('open');
    $(this).text('Close detail');
  }
});
