$(document).on("submit", ".bookmark-form", function(event) {
  var $f = $(this);

  $.post($f.attr('action'), $f.serialize())
    .done(function(data) {
      $f.parents().closest('.sentence').hide('slow');
    })
    .fail(function() {
      alert('Raised error, please try again or contact us administrator');
    });
  return false;
});

$(document).on("click", ".toggle-see-comments", function() {
  var $comment = $(this).parents().closest('.content-wrapper').find('.replies');
  if ($comment.hasClass('hide')) {
    $comment.removeClass('hide')
    $(this).find('.notDisplayed').text('Close');
  }
  else {
    $comment.addClass('hide');
    $(this).find('.notDisplayed').text('See');
  }
  return false;
});
