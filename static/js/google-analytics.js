/* google event */
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-42333150-1', 'lingring.net');
    ga('require', 'linkid', 'linkid.js');    
    ga('send', 'pageview');

    var _gaq = _gaq || [];
    var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
    _gaq.push(['_setAccount', 'UA-42333150-1']);
    _gaq.push(['_setCampSourceKey', 'utm_source']);
    _gaq.push(['_setCampMediumKey', 'utm_medium']);
    _gaq.push(['_setCampContentKey', 'utm_keyword']);
    _gaq.push(['_setCampTermKey', 'utm_keyword']);
    _gaq.push(['_setCampNameKey', 'utm_campaign']);
    _gaq.push(['_trackPageview']);
    _gaq.push(['_require', 'inpage_linkid', pluginUrl]);

    (function(){
        if (!window.addEventListener) {
            window.attachEvent("onload",function() {
                //window.scrollTo(0, 1);
            });
        }
        else {
            window.addEventListener("load",function() {
                //window.scrollTo(0, 1);
            });
        }
})();

$(document).on("click", ".gaevent", function(e) {
    $btn = $(this);
    category = $btn.attr('data-gac');
    name = $btn.attr('data-gan');

    ga('send', 'event', category, name);
    var agt = navigator.userAgent.toLowerCase();
    if (agt.indexOf("firefox") != -1){
        setTimeout(function() {
            if ($btn.attr('type') == 'submit')
                $btn.attr('disabled', true);
            e.currentTarget.form.submit();
        }, 100);
    }
    else if (agt.indexOf("msie") != -1) { return true; }
    else {
        if (e.toElement.tagName == 'A')
            return true;
        setTimeout(function() {
            if ($btn.attr('type') == 'submit')
                $btn.attr('disabled', true);
            $btn.closest('form').submit();
        }, 100);
    }
    return false;
});
