$(document).ready(function() {
  var $charts = $(".chart-data");
  var $event = $(".event-data");

  $.each($charts, function(index, el) {
    $.get($(el).data('url'), function(data) {
      $(el).highcharts(data);
    });
  });

  $.get($event.data('url'), function(data) {
  })
    .done(function(data) {
      $('#calendar').clndr({
        events: data,
        clickEvents: {
          click: function(target) {
          },
          onMonthChange: function(month) {
          }
        },
        doneRendering: function() {
        }
      });
    })
  ;
});

