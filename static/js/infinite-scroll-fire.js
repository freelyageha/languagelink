$(".content_wrapper").infinitescroll({
    navSelector: '#page-nav',
    // selector for the paged navigation
    nextSelector: '#page-nav a',
    // selector for the NEXT link (to page 2)
    itemSelector: '.item',
    // selector for all items you'll retrieve
    animate : false,
    bufferPx: 500,
    loading: {
        finishedMsg: 
            '<div class="well text-center">No more pages to load</div>',
        img: '/static/image/loading.gif'
    }
},

// trigger Masonry as a callback
function( newElements ) {
    // hide new items while they are loading
    var $newElems = $( newElements ).css({ opacity: 0 });
    // ensure that images load before adding to masonry layout
    $newElems.animate({ opacity: 1 });

    var ptn =  /page=\d+/;
    var $nav = $("#page-nav a");
    last_page = parseInt($nav.attr('data'));
    current_url = $nav.attr('href');
    current_page = current_url.match(ptn)[0].split("=")[1];
    next_page = parseInt(current_page) + 1;
    if (parseInt(last_page) >= parseInt(next_page)) {
        $nav.attr('href', current_url.replace(current_page, next_page));
    }
    else {
        $('.content_wrapper').infinitescroll('pause');
    }

    $('.rating-readonly').raty({
        path      : "/static/image/rating",
        numberMax : 5,
        number    : 100,
        readOnly  : true,
        score     : function() {
          return $(this).data('score');
        }
      });
});
