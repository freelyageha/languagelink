function dynamicModal (message, type) {
    type = (type ? type : 'alert-danger');
    var $modal = $('<div>').addClass('modal hide fade');
    var $modal_header = $('<div class="modal-header">').append(
        $('<button class="close">').attr('data-dismiss', 'modal').text('x'))
        .append($('<h4>').text('Done!'));
    var $modal_body = $('<div>').addClass('modal-body').addClass(type)
        .append(message);
    var $modal_footer = $('<div class="modal-footer">').append(
        $('<button class="btn">').attr('data-dismiss', 'modal').text('Close'));
    $modal.append($modal_header, $modal_body, $modal_footer);
    $modal.modal({
        hidden: $modal.remove()
    });
}

$(document).on('click', '.bump-up', function() {
    var url = $(this).data('url');
    $("#modal-confirm-bump").modal();
    $('#modal-confirm-bump').on('shown', function () {
        $('#request-bumpup-form').attr('action', url);
    });
    return false;
});

$(document).on('click', '.radio_link', function() {
    var url = $(this).data('url');
    location.href= url;
});

$(document).on('submit', 'form#request-bumpup-form', function() {
    var url = $(this).attr('action');
    $.ajax({
        url: url,
        type: 'POST',
        data: $(this).serialize(),
        dataType: 'json',
        success: function(res) {
            if (res.status == 'success') {
                var $item = $("#ask_" + res.ask_id);
                var $content_wrapper = $(".content_wrapper");
                $item.hide()
                    .prependTo($content_wrapper)
                    .addClass('alert-success').fadeIn('slow')
                    .removeClass('alert-success', 6000);
                $("#modal-confirm-bump").modal('toggle');
                dynamicModal($('<strong>').text(res.message), 'alert-success');
                $item.find('button.bump-up').remove();
                
                res.used_point
                $pointInfo = $(".pointInfo");
                if ($pointInfo) {
                    $point = $($pointInfo.children()[1])
                    $point.text($point.text() - res.used_point);
                }
            }
            else { 
                $("#modal-confirm-bump").modal('toggle');
                dynamicModal($('<strong>').text(res.message));
            }
        },
        fail: function(res) {
            $("#modal-confirm-bump").modal('toggle');
            dynamicModal($('<strong>').text(res.message));
        }
    });
    return false;
});

$(document).on('click', '.chatroom_item', function(){
    location.href = $(this).attr('data-url');
});

/* popover event - show mini profile */
$(document).on('click', '*[data-poload]', function(){
    var e=$(this);
    if (!$(e.parent().find('.popover')).css('display')) {
        $.get(e.data('poload'),function(d) {
            e.popover({
                title: e.attr('popover-title'),
                content: d,
                delay: {show: 500, hide: 100 }
            }).popover('show');
        });
    }
    popovered = $($(".popover").parent().find('a'))
    popovered.popover('destroy');
});


/* modal event - post */
$(document).on('click', '[data-target="#modal-post"]', function(e) {
  e.preventDefault();
  var url = $(this).attr('href');
  var target = $(this).attr('data-target');
  var topic_title = $(this).data('title');
  var topic_id = $(this).data('id');

  if (url.indexOf('#') == 0) {
    $(url).modal('open');
    if (topic_title) {
      $(target + " .modal-body").find("input#id_title").val(topic_title);
      $(target + " .modal-body").find("input#id_topic").val(topic_id);
    } else {
      $(target + " .modal-body").find("input#id_title").val('');
      $(target + " .modal-body").find("input#id_topic").val('');
    }
  } else {
    $.get(url, function(data) {
      $(target + " .modal-body").extend(data);
    }).success(function() { 
    });
  }
});
$(document).on('hidden', '#modal-post', function() {
    $('[data-target="#modal-post"]').blur();
})
$(document).on('submit', '#post_form', function() {
    $context = $(this).find("#id_context");
    if (!$context.val() || $context.val().length < 15) {
        alert("Total letter: " + $context.val().length + "\nFor good writing practice, you must write more than 15 letters!!");

        $context.focus();
        return false;
    }
    $(this).find("button[type='submit']").attr('disabled', true);
    ga('send', 'event', "Retention", "Write");
});

/* modal event - ring */
$(document).on('submit', '#request-tutor-form', function() {
    $f = $(this);
    $.ajax({
        url: $f.attr('action'),
        type: 'POST',
        data: $f.serialize(),
        dataType: 'json',
        success: function(res) {
            $('#modal-ring').modal('hide')
            var $btn = $('#modal-ring-btn');
            $btn.parent().addClass('muted')
            $btn.text('Be My Tutor (Sent)').attr('href', '');
        },
        fail: function(res) {
        }
    });
    return false;
});

$(document).on("submit", "#topic-read-form", function() {
  var $f = $(this);
  $.ajax({
    url: $f.attr('action'),
    type: 'POST',
    data: $f.serialize(),
    dataType: 'json',
    success: function(res) {
      var has_topic = res['topic'];
      console.log(has_topic);
      if (has_topic) {
        $("#topic-read-form").attr('action', res['add_reader_url']);
        $("#create-topic-btn").data('title', res['title']);
        $("#create-topic-btn").data('id', res['id']);
        $(".topic-author").text(res['author']);
        $(".topic-author-language").text(res['author_language']);
        $(".topic-title").text('"' + res['title'] + '"');
      } else {
        $f.parents().closest('.topicBox').remove();
        $("#no-unread-topic").show().removeClass('hidden');
      }
    },
    fail: function(res) {
      alert('Something wrong during dismiss topic!');
    }
  });
  return false;
})


/* modal event - follow */
$(document).on('submit', '#request-follow-form', function() {
    $f = $(this);
    $.ajax({
        url: $f.attr('action'),
        type: 'POST',
        data: $f.serialize(),
        dataType: 'json',
        success: function(res) {
            $('#modal-follow').modal('hide')
            var $btn = $('#modal-follow-btn');
            $btn.parent().addClass('muted accepted')
            $btn.text('Interesting Student').attr('href', '');
        },
        fail: function(res) {
        }
    });
    return false;
});



function get_notification(url, opponent_img_url, oppnent_ask_list_url) {
$.ajax({
    url: url,
    dataType: 'json',
    success: function(noti){
        if (noti['chatrooms'])
            $("#message_badge").text(noti['chatrooms']).show();
        else
            $("#message_badge").hide();

        if (noti['corrections'])
            $("#post_badge").text(noti['corrections']).show();
        else
            $("#post_badge").hide();

        if (noti['correction_list']) {
            $('.alert-info').removeClass('alert-info');
            list = (noti['correction_list']);
            for (index in list) {
                words = list[index].split(':');
                $("#ask_" + words[2]).addClass('alert-info');
            }
        }
        else if (noti['chatroom_list']) {
            $('.alert-info').removeClass('alert-info');
            list = (noti['chatroom_list']);
            for (index in list) {
                words = list[index].split(':');
                $("#chat_" + words[2]).addClass('alert-info');
            }
        }
        else if (noti['messages']) {
            $wrapper = $("#chat_wrapper");
            messages = noti['messages'];
            for (var index in messages) {
                $msg = $("<div>").addClass("clear media pull-left");
                $sender_img = $("<a>").attr("href", oppnent_ask_list_url).append($("<img/>").addClass("media-object media-object-32 pull-left").attr("src", opponent_img_url));
                $content = $("<div>").addClass("media-body pull-left");
                $content_header = $("<span>").addClass("chat-header");
                $content_header.text('5secs');
                $content_body = $("<span>").addClass("chat-body");
                $content_body.text(messages[index]);
                $content.append($content_header);
                $content.append("<br>");
                $content.append($content_body);
                $msg.append($sender_img).append($content);
                $wrapper.append($msg);
            }
            window.scroll(0, document.documentElement.scrollHeight);
        }

    }
});
}

$(document).ready(function() {
    var $modal_help = $('.modal-help');
    if ($modal_help.length) {
        var id = $modal_help.attr('id');
        var cookie = getCookie(id);
        if (cookie==null || cookie=="") {
            $modal_help.modal();
        }
    }

    var $has_introjs = $('.hi');
    if ($has_introjs.length) {
      var id = $has_introjs.attr('id');
      var cookie = getCookie(id);
      if (cookie==null || cookie=="") {
        introJs().setOptions({
            showStepNumbers: false
          })
          .start()
          .oncomplete(function() {
            setCookie(id, 'introjs-hide', 3650);
          })
          .onexit(function() {
            setCookie(id, 'introjs-hide', 3650);
          });
      }
    }

    $modal_help.on('hidden', function () {
        var id = $(this).attr('id');
        var check_show = $(this).find('input.never-show-check');
        var un_cookied = $(this).hasClass('un-cookied');

        if (!un_cookied &&
            (!check_show || (check_show && check_show.is(":checked"))))
        {
            setCookie(id, 'modal-hide', 3650);
        }
    });

    if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
        $(":input").focus( function() {
            $(".navbar-fixed-top").css("position", "absolute");
        });
        $(":input").blur( function() {
            $(".navbar-fixed-top").css("position", "fixed");
        });
    }

    setInterval(function(){$(".point-message").hide("slow");}, 4000);

    $('.rating-readonly').raty({
        path      : "/static/image/rating",
        numberMax : 5,
        number    : 100,
        readOnly  : true,
        score     : function() {
          return $(this).data('score');
        }
      });
});

function setCookie(cName, cValue, cDay){
  var expire = new Date();
  expire.setDate(expire.getDate() + cDay);
  cookies = cName + '=' + escape(cValue) + '; path=/ ';
  if(typeof cDay != 'undefined') cookies += ';expires=' + expire.toGMTString() + ';';
  document.cookie = cookies;
}

function getCookie(cName) {
  cName = cName + '=';
  var cookieData = document.cookie;
  var start = cookieData.indexOf(cName);
  var cValue = '';
  if(start != -1){
       start += cName.length;
       var end = cookieData.indexOf(';', start);
       if(end == -1)end = cookieData.length;
       cValue = cookieData.substring(start, end);
  }
  return unescape(cValue);
}
