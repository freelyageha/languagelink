window.setTimeout(function() { $(".alert-message").alert('close'); }, 3000);

function completeInvite(res, from) {
  var csrftoken = getCookie('csrftoken');
  var userId = []
  $.each( res['to'], function( index, uid ) {
    userId.push(uid.toString());
  });

  $.ajax({
        url: '/user/' + from + '/invite/add/',
        dataType: 'json',
        type: 'post',
        data: {
            'to': userId,
            'csrfmiddlewaretoken': csrftoken
        },
        success: function(result) {
          /* response
          Object {
            request: "412636805549601",
            to: Array[1], e2e: "{"submit_0":1391981336189}"}
          */
        },
        error: function(result) {
        }
  });
}

function FacebookInviteFriends(from) {
  FB.ui({
    method: 'apprequests',
    filters: ['app_non_users'],
    data: 'code=invited&from=' + from,
    message: 'Choose your friends to send invitation.'
  }, function (res) {
    if (res['to']) { 
      completeInvite(res, from);

      modalId = "modal-help-invite";
      var cookie = getCookie(modalId);
      var $modal_help = $("#" + modalId);
      if (cookie==null || cookie=="") $modal_help.modal();

      $modal_help.on('hidden', function () {
          id = $(this).attr('id');
          setCookie(id, 'modal-hide', 3650);
      });
    }
  });
}

$(document).on("click", ".invite_friends", function() {
  var from = $(this).data('from')
  FacebookInviteFriends(from);
  return false;
});

$(document).ready(function () {
  $modal_newbie_reply_rate = $('#modal-newbie-reply-rate');
  $modal_low_reply_rate = $('#modal-low-reply-rate');

  newbie_reply_cookie = getCookie('modal-newbie-reply-rate');
  if (newbie_reply_cookie==null || newbie_reply_cookie=="") {
    $modal_newbie_reply_rate.modal();

    $modal_newbie_reply_rate.on('hidden', function () {
        id = $(this).attr('id');
        setCookie(id, 'modal-hide', 365);
    });
  }
  else if ($modal_low_reply_rate) {
    low_reply_cookie = getCookie('modal-low-reply-rate');
    if (low_reply_cookie==null || low_reply_cookie=="") {
      $modal_low_reply_rate.modal();

      $("#lowReplyNeverShow").on('click', function() {
        setCookie('modal-low-reply-rate', 'modal-hide', 365);
      });
    }
  }
})
