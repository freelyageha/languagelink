$(document).ready(function() {
    $(".feedback_form ul li input").on("change", function() {
        $f = $(this).closest("form");
        $.ajax({
            url: $f.attr('action'),
            type: 'POST',
            data: $f.serialize(),
            dataType: 'json',
            async: 'true',
            success: function(res) {
                $f.find(".feedquestion").hide();
                $f.find(".feedresult").hide();
                $f.find(".thanks").show();
            },
            fail: function(res) {
                $f.find(".wrong").show();
            }
        });
    });

    $(".disabled").click(function() {
        var data = $(this).data('alert');
        $("#" + data).show();
    });
});

