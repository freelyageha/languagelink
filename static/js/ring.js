var decrease_notification_count = function ($el) {
  if ($el) {
    var notifications_count = $el.text();
    notifications_count = notifications_count - 1;
    if (notifications_count < 1)
      $el.remove();
    else
      $el.text(notifications_count);
  }
};


$(document).on("submit", "#quit_talk", function() {
  var $f = $(this);
  $.post($f.attr('action'), $f.serialize())
    .done(function(data) {
      has_notification = data['has_notification'];
      $f.parent().parent().hide('slow').remove();
      decrease_notification_count($("#talk_count"));

      if (has_notification) {
        var $talk_notifications = $("#talk_notifications");
        var $ring_notifications = $("#message_badge");
        decrease_notification_count($talk_notifications);
        decrease_notification_count($ring_notifications);
      }
    })
    .fail(function(data) { });
  return false;
});

$(document).on("click", ".unring", function() {
    var $alert = $(this).parent().parent().find('.disconnect_ring');
    $alert.removeClass('hide').show();
});

$(document).on("click", ".cancel_disconnect", function() {
    var $alert = $(this).parent().parent();
    $alert.addClass('hide').hide();
    return false;
});

$(document).on("submit", ".confirm-tutor", function() {
    var $f = $(this);
    $.ajax({
      url: $f.attr('action'),
      dataType: 'json',
      type: 'post',
      data: $f.serialize(),
      success: function(result) {
          $f.hide();
          var $tutor_notifications = $("#tutor_notifications");
          var $ring_notifications = $("#message_badge");
          decrease_notification_count($tutor_notifications);
          decrease_notification_count($ring_notifications);
      },
      error: function(result) {
          alert('Error! please contact to the site manager.');
      }
    })
    return false;
})
