var socket = io.connect('http://alpha.lingring.net:8080');
var chatroomId = md5($("#chat_room_id").data('roomid'));

$(document).ready(function() {
    socket.on('connect', function(){
      socket.emit('adduser', chatroomId);
    });

    socket.on('updatechat', function (data) {
      var _data = data.split("::")
      var user = _data.shift();
      var ctime = _data.shift();
      if (user != $("#send_message").find("input[name='user']").val()) {
        $("#message_receive").tmpl({
            'data': _data.join("::"),
            'time': ctime,
          }).appendTo("#chat_wrapper");
      } else {
        $("#message_send").tmpl({
            'data': _data.join("::"),
            'time': ctime,
          }).appendTo("#chat_wrapper");
      }
      window.scroll(0, document.documentElement.scrollHeight);
    });

    $("#id_context").focus();
    window.scroll(0, document.documentElement.scrollHeight);

    $("#send_message").on("submit", function(event) {
        event.preventDefault();
        $context = $("#id_context");
        $form = $(this);
        $.ajax({
            url: $form.attr('action'),
            dataType: 'json',
            type: 'post',
            data: $form.serialize(),
            beforeSend: function(xhr) {
                $form.hide();
                $(".sending").show();
            },
            success: function(result){
                var message = result['context'];
                var user = $form.find('input[name="user"]').val();
                $("#message_send").tmpl({
                    'data': message,
                    'time': result['ctime'],
                  }).appendTo("#chat_wrapper");
                $("#id_context").val('');
                window.scroll(0, document.documentElement.scrollHeight);
                socket.emit(
                  'sendchat', chatroomId,
                  (user + "::" + result['ctime'] + "::" + message )
                )
                $form[0].reset();
            },
            error: function(result) {
            },
            complete: function() {
                $form.show();
                $(".sending").hide();
            }
        });
        return false;
    });
});
