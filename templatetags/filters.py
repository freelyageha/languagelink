from django import template
from django.conf import settings
from django.utils.encoding import force_text
from django.utils.safestring import mark_safe
from django.core.exceptions import ObjectDoesNotExist

from lang.models import Poll, UserProfile

register = template.Library()

@register.filter(is_safe=True)
def get_representative(value, user):
    try:
        return value.exclude(username=user.username)[0]
    except:
        return user

@register.filter(is_safe=True)
def is_chat_include(notifications, chatroom):
    for _chatroom in notifications:
        if chatroom == _chatroom.talk:
            return True
    return False

@register.filter(is_safe=True)
def add_str(value, arg):
    return value + str(arg)

@register.filter(is_safe=True)
def contains(container, item):
    if item in container:
        return True
    return False

@register.filter(is_safe=True)
def has_corrector(corrections, user):
    return corrections.filter(user=user).exists()

@register.filter(is_safe=True)
def get_corrected_sentences(sentence, corrector):
    return sentence.child.filter(user=corrector).reverse()

@register.filter(is_safe=True)
def has_notified_reply(ask, user):
    return ask.reply_notifications().filter(receiver=user).exists()

@register.filter(is_safe=True)
def help_count(tutor, student):
    return student.ask_set.filter(correction__user=tutor).count()

@register.filter(is_safe=True)
def included_user_query(queryset, user):
    q = queryset.filter(user=user)
    return q[0] if q.exists() else None

@register.filter(is_safe=True)
def included_user_queryset(queryset, user):
    return queryset.filter(user=user)

from emailconfirmation.models import EmailAddress
@register.filter(is_safe=True)
def is_confirmed_email(user, email):
    if user.is_anonymous():
        return True
    current_email = user.emailaddress_set.get(email=email)
    if current_email:
        return current_email.verified
    return False


@register.filter(is_safe=True)
def get_bar_color(level):
    rate = 'danger'
    if level.name == 'Excellent':
        color = 'success'
    elif level.name == 'Very Good':
        rate = 'info'
    elif level.name == 'Good':
        rate = 'warning'
    return rate

@register.filter(is_safe=True)
def bar_color_for_post_count(posts_count, level):
    rate = 0
    if level.name == 'Excellent':
        rate = 100
    elif level.name == 'Very Good':
        rate = 100 if posts_count >= 10 else (posts_count-5)/5*100
    elif level.name == 'Good':
        rate = 100 if posts_count >= 5 else (posts_count-3)/2*100
    elif level.name == 'Normal':
        rate = 100 if posts_count >= 3 else (posts_count)/3*100
    return rate

@register.filter(is_safe=True)
def str_to_date(str_date, format):
    from datetime import datetime
    try:
        return datetime.strptime(
                str_date, "%Y-%m-%d").strftime("%m. %d. %Y")
    except:
        return str_date

@register.filter(is_safe=True)
def filter_score_level(score):
    if score <= 20:
        return 'error'
    elif score <= 40:
        return 'warning'
    elif score <= 60:
        return ''
    elif score <= 80:
        return 'info'
    return 'success'


@register.filter(is_safe=True)
def is_polled(event, user):
    polls = Poll.objects.filter(
            event_id=event.id, user=user)
    if not polls.exists():
        return True
    return False

@register.filter(is_safe=True)
def from_notification(notifications, user):
    if notifications.filter(sender__in=[user]).exists():
        return True
    return False

@register.filter(is_safe=True)
def is_my_student(user, student):
    student = user.my_students().filter(student=student)
    if student.exists():
        return True
    return False

@register.filter(is_safe=True)
def is_my_tutor(user, tutor):
    tutor = user.my_tutors().filter(tutor=tutor)
    if tutor.exists():
        return True
    return False

@register.filter(is_safe=True)
def to_me(notification, user):
    if notification and notification[0].receiver == user:
        return True
    return False

@register.filter(is_safe=True)
def profile_photo(user_id, _type):
    from easy_thumbnails.files import get_thumbnailer
    try:
        user = UserProfile.objects.get(id=user_id)
        thumb_url = get_thumbnailer(user.profile_photo)[_type].url
        return thumb_url
    except ObjectDoesNotExist:
        if _type == 'post':
            return '/static/image/non_profile_photo.64x64.jpg'
        else:
            return '/static/image/non_profile_photo.64x64.jpg'
    except:
        return '/static/image/non_profile_photo.64x64.jpg'


@register.filter(is_safe=True)
def get_user(user_id):
    try:
        return UserProfile.objects.get(id=user_id)
    except ObjectDoesNotExist:
        return None
    except:
        return None


@register.filter(is_safe=True)
def social_comment_start_date(startdate):
    from datetime import datetime
    t = datetime.now().strftime("Y.m.d")
    if t > startdate:
        return True
    return False

@register.filter(is_safe=True)
def my_askid_for_topic(topic_id, user):
    ask = user.ask_set.get(topic__id=topic_id)
    return ask.id

