from django.conf.urls import patterns, url
from django.conf import settings
from django.views.generic import TemplateView

from lang.views import (
        Home, GoogleWebMaster, GoogleRobot, GoogleSitemap, Canvas,
        NewbieExample, ContactView, ContactCompleteView )
from lang.views.ask import (
        AskPostList, AskCorrectionList, AskPost, AskView, AskLanguageList,
        AskMyStudentList, AskUpdate, AskDelete, AskBump, CorrectionCreate)
from lang.views.correction import SentenceUpdate
from lang.views.comment import (
    CommentCreate, CommentUpdate, OverallCommentCreate, OverallCommentUpdate)
from lang.views.talk import TalkRedirection, TalkOpen, TalkSendMessage, TalkList, TalkQuit
from lang.views.ring import RingList, RingCreate, RingUpdate, RingDelete
from lang.views.quiz import QuizBookList, QuizBookUpdate, QuizBookDetail, QuizFeedback
from lang.views.invite import InviteCreate
from lang.views.point import PointList
from lang.views.poll import PollCreate
from lang.views.helps import HelpView, LegacyHelpView, StaticHelpView
from lang.views.notification import NotificationView, NotificationRead
from lang.views.bookmark import BookmarkList, BookmarkCreateOrDelete
from lang.views.topic import TopicList, TopicCreate, TopicUpdate, TopicDelete, TopicAddReader
from lang.views.privacy import PrivacyView
from lang.views.rank import WeeklyRankList, MonthlyRankList, AllRankList
from lang.views.growth import Growth, ScoreGrowth, EventGrowth


urlpatterns = patterns('',
    url(r'^$', Home.as_view(), name='home'),
    url(r'^googlee367e4e16cd0841d.html$',
        GoogleWebMaster.as_view(), name='google_web_master'),
    url(r'^robots.txt$', GoogleRobot.as_view(), name='google_robot'),
    url(r'^sitemap.xml$', GoogleSitemap.as_view(), name='google_sitemap'),
    url(r'^canvas/$', Canvas.as_view(), name='canvas'),
    url(r'^example/$', NewbieExample.as_view(), name='example'),

    url(r'^contact/$', ContactView.as_view(), name='contact'),
    url(r'^thanks/$', ContactCompleteView.as_view(), name='thanks'),

) + patterns('lang.views.ask',
    url(r'^post/$', AskPost.as_view(), name='ask_post'),
    url(r'^post/(?P<pk>\d+)/$', AskView.as_view(), name='ask_view'),
    url(r'^post/(?P<pk>\d+)/correct/(?P<corrector_name>[-\.\w]+)/$',
        AskView.as_view(), name='ask_correction_view'),
    url(r'^post/(?P<pk>\d+)/bump/$', AskBump.as_view(), name='ask_bump'),
    url(r'^post/(?P<pk>\d+)/update/$', AskUpdate.as_view(), name='ask_update'),
    url(r'^post/(?P<pk>\d+)/delete/$', AskDelete.as_view(), name='ask_delete'),

    url(r'^post/(?P<pk>\d+)/correction/$', CorrectionCreate.as_view(),
        name='correction_create'),

    url(r'^user/(?P<screen_name>[-\.\w]+)/$', 
        AskPostList.as_view(), name='ask_post_list'),
    url(r'^user/(?P<screen_name>[-\.\w]+)/post/$', 
        AskPostList.as_view(), name='ask_post_list'),
    url(r'^user/(?P<screen_name>[-\.\w]+)/correction/$', 
        AskCorrectionList.as_view(), name='ask_correction_list'),

    url(r'^language/$', AskLanguageList.as_view(), name='ask_language_list'),
    url(r'^language/(?P<language>[-\w]+)/$', 
        AskLanguageList.as_view(), name='ask_language_list'),
    url(r'^mystudents/$', AskMyStudentList.as_view(), name='ask_student_list'),

) + patterns('lang.views.sentence',
    url(r'^sentence/(?P<pk>\d+)/update/$',
        SentenceUpdate.as_view(), name='sentence_update'),

) + patterns('lang.views.comment',
    url(r'^comment/create/$', CommentCreate.as_view(), name='comment_create'),
    url(r'^comment/(?P<pk>\d+)/update/$', CommentUpdate.as_view(), name='comment_update'),
    url(r'^overall_comment/create/$',OverallCommentCreate.as_view(), name='overall_comment_create'),
    url(r'^overall_comment/(?P<pk>\d+)/update/$', OverallCommentUpdate.as_view(), name='overall_comment_update'),

) + patterns('lang.views.talk',
    url(r'^talk/$', TalkList.as_view(), name='talk_list'),
    url(r'^talk/searchroom/$', TalkRedirection.as_view(), name='search_chatroom'),
    url(r'^talk/send/$', TalkSendMessage.as_view(), name='send_chat_message'),
    url(r'^talk/open/(?P<pk>\d+)/$', TalkOpen.as_view(), name='open_chatroom'),
    url(r'^talk/quit/(?P<pk>\d+)/$', TalkQuit.as_view(), name='quit_chatroom'),

) + patterns('lang.views.ring',
    url(r'^ring/$', RingList.as_view(), name='ring_list'),
    url(r'^ring/create/$', RingCreate.as_view(), name='ring_create'),
    url(r'^ring/(?P<pk>\d+)/update/$', RingUpdate.as_view(), name='ring_update'),
    url(r'^ring/(?P<pk>\d+)/delete/$', RingDelete.as_view(), name='ring_delete'),

) + patterns('lang.views.quiz',
    url(r'^user/(?P<screen_name>[-\.\w]+)/quiz/$', 
        QuizBookList.as_view(), name='quizbook_list'),
    url(r'^user/(?P<screen_name>[-\.\w]+)/quiz/(?P<date>\d{4}-\d{2}-\d{2}(-\d+)?)/$', 
        QuizBookUpdate.as_view(), name='quizbook_update'),
    url(r'^user/(?P<screen_name>[-\.\w]+)/quiz/(?P<date>\d{4}-\d{2}-\d{2}(-\d+)?)/result/$',
        QuizBookDetail.as_view(), name='quizbook_detail'),
    url(r'^quiz/(?P<pk>\d+)/feedback/$', QuizFeedback.as_view(), name='quiz_feedback'),

) + patterns('lang.views.point',
    url(r'^user/(?P<screen_name>[-\.\w]+)/points/$', 
        PointList.as_view(), name='point_list'),

) + patterns('lang.views.invite',
    url(r'^user/(?P<screen_name>[-\.\w]+)/invite/add/$',
        InviteCreate.as_view(), name='invite_add'),
    #url(r'^user/(?P<screen_name>[-\.\w]+)/invites/$',
    #    InviteList.as_view(), name='invite'),

) + patterns('lang.views.invite',
    url(r'^user/(?P<screen_name>[-\.\w]+)/growth/$', Growth.as_view(), name='growth'),
    url(r'^user/(?P<pk>\d+)/growth/score/$', ScoreGrowth.as_view(), name='score_of_growth'),
    url(r'^user/(?P<pk>\d+)/growth/event/$', EventGrowth.as_view(), name='event_of_growth'),

) + patterns('lang.views.poll',
    url(r'^poll/(?P<event_id>\d+)/$', 
        PollCreate.as_view(), name='poll_create'),

) + patterns('lang.views.helps',
    url(r'^help/$', HelpView.as_view(), name='helps'),

    url(r'^help/what-can-i-write-about/$',
        TemplateView.as_view(template_name="lang/help/post/about.html"),
        name='helps_post_about'),
    url(r'^help/what-will-happen-after-i-write-a-post/$',
        TemplateView.as_view(template_name="lang/help/post/will_happen.html"),
        name='helps_post_will_happen'),
    url(r'^help/i-want-to-learn-more-than-two-foreign-languages/$',
        TemplateView.as_view(template_name="lang/help/post/more_language.html"),
        name='helps_post_more_language'),

    url(r'^help/(?P<slug>[\w-]+)/$', StaticHelpView.as_view(), name='static_helps'),
    # below urls are legacy
    url(r'^help/(?P<pk>\d+)/$', LegacyHelpView.as_view(), name='helps'),

) + patterns('lang.views.notification',
    url(r'^notifications/$', NotificationView.as_view(), name='notifications'),
    url(r'^notifications/(?P<pk>\d+)/$',
        NotificationRead.as_view(), name='notification_update'),

) + patterns('lang.views.bookmark',
    url(r'^user/(?P<screen_name>[-\.\w]+)/notes/$', 
        BookmarkList.as_view(), name='bookmark_list'),
    url(r'^notes/create_or_delete/$', BookmarkCreateOrDelete.as_view(),
        name='bookmark_create_or_delete')

) + patterns('lang.views.bookmark',
    url(r'^rank/$', AllRankList.as_view(), name='all_rank'),
    url(r'^rank/weekly/$', WeeklyRankList.as_view(), name='weekly_rank'),
    url(r'^rank/monthly/$', MonthlyRankList.as_view(), name='monthly_rank')

) + patterns('lang.views.topic',
    url(r'^topics/(?P<language>[-\w]+)/$', 
        TopicList.as_view(), name='topic_list'),
    url(r'^topics/(?P<language>[-\w]+)/create/$', 
        TopicCreate.as_view(), name='topic_create'),
    url(r'^topics/(?P<language>[-\w]+)/(?P<pk>\d+)/update/$', 
        TopicUpdate.as_view(), name='topic_update'),
    url(r'^topics/(?P<language>[-\w]+)/(?P<pk>\d+)/delete/$', 
        TopicDelete.as_view(), name='topic_delete'),
    url(r'^topics/(?P<language>[-\w]+)/(?P<pk>\d+)/read/$', 
        TopicAddReader.as_view(), name='topic_add_reader'),

) + patterns('lang.views.legal',
    url(r'^privacy/$', PrivacyView.as_view(), name='privacy'),

) + patterns('',
    url(r'^%s(.*)$' % settings.MEDIA_URL[1:],
        'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }, 'static'),
)
