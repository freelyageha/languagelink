from datetime import datetime, timedelta
from dateutil import tz

def format_second_to_clock(seconds):
    if seconds < 0:
        seconds = 86400 + seconds
    """Convert a time in seconds to a string like '15:22:01'"""
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)

    if h:
        return "%d hour%s" % (h, 's' if h > 1 else '')
    elif m:
        return "%d min%s" % (m, 's' if m > 1 else '')
    elif s:
        return "%d sec%s" % (s, 's' if s > 1 else '')
    else:
        return "now"

def time_to_seconds(timeobj):
    """Convert a time to seconds"""
    return (timeobj.hour*60*60) + \
            (timeobj.minute*60) + timeobj.second

def get_elapsed_time(time):
    now = datetime.utcnow()
    delta = timedelta(days=1)
    if now - delta <= time.replace(tzinfo=None):
        diff = time_to_seconds(now) - \
                time_to_seconds(time)
        return format_second_to_clock(diff)
    elif time.year < now.year:
        return time.strftime("%-d %B %y")
    else:
        return time.strftime("%-d %B")


def merge_zero_list_to_single_list(l):
    return l if len(l) > 1 else [0] + l
