from django import http

import json

from django.views.generic.base import RedirectView, ContextMixin, View
from django.views.generic.edit import FormView
from django.views.decorators.csrf import csrf_exempt
from django.core.urlresolvers import reverse
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden
from django.views.generic.base import TemplateView
from django.contrib import messages
from django.shortcuts import render
from django import forms
from django.http import Http404

from lang.decorators import active_required
from lang.forms import AskForm
from lang.models import UserProfile, Event
from lang.mail import LingRingMailman


class Home(View):
 
    def get(self, request, *args, **kwargs):
        user = self.request.user
        if user.is_authenticated():
            if user.is_new or not user.screen_name:
                return HttpResponseRedirect(
                        reverse('profile_setting', args=(user.id, )))
            return HttpResponseRedirect(
                    reverse('ask_language_list', args=(user.get_native_language().name,)))
        return render(request, 'registration/login.html')


class LangExtraContext(ContextMixin):

    @method_decorator(active_required)
    def dispatch(self, request, *args, **kwargs):
        return super(LangExtraContext, self).dispatch(
                request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        user = self.request.user
        profile_user = None
        if self.kwargs.has_key('screen_name'):
            profile_user = UserProfile.objects.get(
                    screen_name=self.kwargs['screen_name'],
                    )

        context = super(
                LangExtraContext, self).get_context_data(**kwargs)
        context.update({
            'user': self.request.user,
            'profile_user': profile_user,
            'request': self.request,
            'main_menu': self.request.path.split("/")[1],
            'ask_form': AskForm(user) if user.is_authenticated() else "",
            'events': Event.objects.filter(active=True),
        })
        return context


class Canvas(View):
    permanent = False

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(Canvas, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return render(request, 'canvas.html')


class NewbieExample(TemplateView, LangExtraContext):
    permanent = False
    template_name = "lang/help/example.html"

    def get_context_data(self, **kwargs):
        user = self.request.user
        if user.is_authenticated():
          user_learning_language = user.get_learning_language().name
          next_url = reverse('ask_post')
        else:
          user_learning_language = self.request.GET.get('language', 'english')
          next_url = reverse('ask_language_list', args=(user_learning_language,))

        context = super(NewbieExample, self).get_context_data(**kwargs)
        context.update({
          'user_learning_language': user_learning_language,
          'next_url': next_url,
        })
        return context


class GoogleWebMaster(TemplateView):
    permanent = False
    template_name = "googlee367e4e16cd0841d.html"


class GoogleRobot(TemplateView):
    permanent = False
    template_name = "robots.txt"


class GoogleSitemap(TemplateView):
    permanent = False
    template_name = "sitemap.xml"


class JSONResponseMixin(object):
    def render_to_response(self, context):
        return self.get_json_response(self.convert_context_to_json(context))

    def get_json_response(self, content, **httpresponse_kwargs):
        return http.HttpResponse(content,
                                 content_type='application/json',
                                 **httpresponse_kwargs)

    def convert_context_to_json(self, context):
        return json.dumps(context)


class MessageMixin(object):
    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(MessageMixin, self).delete(request, *args, **kwargs)

    def form_valid(self, form):
        messages.success(self.request, self.success_message)
        return super(MessageMixin, self).form_valid(form)


class AjaxableResponseMixin(object):
    def render_to_json_response(self, context, **response_kwargs):
        data = json.dumps(context)
        response_kwargs['content_type'] = 'application/json'
        return HttpResponse(data, **response_kwargs)

    def form_invalid(self, form):
        response = super(AjaxableResponseMixin, self).form_invalid(form)
        if self.request.is_ajax():
            return self.render_to_json_response(form.errors, status=400)
        else:
            return response

    def form_valid(self, form):
        response = super(AjaxableResponseMixin, self).form_valid(form)
        if self.request.is_ajax():
            data = {
                'pk': self.object.pk,
            }
            return self.render_to_json_response(data)
        else:
            return response


class ContactForm(forms.Form):
    name = forms.EmailField(
            widget=forms.TextInput(attrs={'type': 'email'})
            )
    message = forms.CharField(
            widget=forms.Textarea(attrs={
                'rows': '3',
                'placeholder': 'Any questions? or What my I help you?',
                'style': 'width:97%;margin:0 auto 10px;',
                })
            )

    def send_email(self):
        mailman = LingRingMailman()
        mailman.contact(self)


class ContactView(FormView, LangExtraContext):
    template_name = 'lang/help/any-trouble-with-login.html'
    form_class = ContactForm
    success_url = '/thanks/'

    def dispatch(self, request, *args, **kwargs):
        user = request.user
        if request.method == 'GET' and user.is_authenticated():
            return HttpResponseRedirect(reverse('static_helps',
                args=('contact-us',)))

        return super(ContactView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        form.send_email()
        return super(ContactView, self).form_valid(form)


class ContactCompleteView(TemplateView):
    template_name = 'lang/help/contact-thanks.html'


class AuthorRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        result = super(AuthorRequiredMixin, self).dispatch(request, *args, **kwargs)
        if self.object.user != self.request.user:
            return HttpResponseForbidden('You does not have permission for modify this topic.')
        return result


