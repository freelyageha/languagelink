# -*- coding: utf-8 -*-
import re, json
from itertools import chain
from diff_match_patch import *

from django.http import Http404
from django.shortcuts import get_object_or_404
from django.views.generic import FormView
from django.views.generic import CreateView, UpdateView, \
        DeleteView, ListView, DetailView

from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.utils.translation import ugettext_lazy

from django.db import transaction
from django import forms
from django.db.models import Q, Count, Max

from lang.models import (Ask, Sentence, UserProfile, Comment, Correction,
        Language, Notification, Ring, OverallComment, Tag, RankPeriod, Rank, Rating)
from lang.views import LangExtraContext, MessageMixin, JSONResponseMixin
from lang.mail import LingRingMailman
from lang.forms import AskForm
from lang.choices import POINT_VALUES

from datetime import date, timedelta
from isoweek import Week

class AskPostList(ListView, LangExtraContext):
    model = Ask
    context_object_name = 'ask_list'
    template_name='lang/ask/list.html'
    paginate_by = 10

    def get_queryset(self):
        order_by = self.request.GET.get('order', '')
        user = get_object_or_404(UserProfile,
                screen_name=self.kwargs['screen_name'],
                )
        queryset = Ask.objects.filter(
                user=user, language=user.get_learning_language()
                )
        if user == self.request.user:
            if order_by:
                result_list = queryset
            else:
                notifications = Notification.objects.filter(
                    receiver=user, post__in=queryset)
                notied_ask_list = queryset.filter(
                    notification__in = notifications
                    ).annotate(
                    latest_mtime=Max('notification__mtime')
                    ).order_by('-latest_mtime').distinct()
                unnotied_ask_list = queryset.filter(
                    ~Q(notification__in = notifications)
                    ).distinct()
                result_list = list(
                    chain(notied_ask_list, unnotied_ask_list)
                    )
            return result_list
        else:
            queryset = queryset.filter(is_published=True)
            return queryset

    def get_context_data(self, *args, **kwargs):
        context = super(AskPostList, self).get_context_data(
                *args, **kwargs)
        user = self.request.user
        profile_user = get_object_or_404(UserProfile,
                screen_name=self.kwargs['screen_name'],
                )
        op_user = user if not user.is_anonymous() else None

        is_my_tutor = Ring.objects.filter(
                tutor=profile_user, student=op_user, status='accepted'
                ).exists()
        is_my_student = Ring.objects.filter(
                student=profile_user, tutor=op_user, status='accepted'
                ).exists()
        ring = Ring.objects.filter(
                tutor=profile_user, student=op_user
                )
        ring = ring[0] if ring.exists() else None 
        is_follower = Ring.objects.filter(
                student=profile_user, tutor=op_user, follow=True
                ).exists()

        context.update({
            'nav_me': 'post',
            'profile_user': profile_user,
            'is_my_tutor': is_my_tutor,
            'is_my_student': is_my_student,
            'ring': ring,
            'is_follower': is_follower,
            'noti_params': '?filter=post' if profile_user == user else '',
            })
        return context


class AskCorrectionList(ListView, LangExtraContext):
    model = Ask
    context_object_name = 'correction_list'
    template_name='lang/ask/correction.html'
    paginate_by = 10

    def get_queryset(self):
        user = get_object_or_404(UserProfile,
                screen_name=self.kwargs['screen_name'],
                )
        queryset = Ask.objects.filter(correction__user=user)

        if user == self.request.user:
            notifications = Notification.objects.filter(
                    receiver=user, post__in=queryset, is_new=True
                    )
            result_list = list(chain(
                queryset.filter(notification__in = notifications).distinct(),
                queryset.filter(~Q(notification__in = notifications)).distinct()
                ))
            return result_list
        else:
            return queryset

    def get_context_data(self, *args, **kwargs):
        context = super(AskCorrectionList, self).get_context_data(
                *args, **kwargs)
        profile_user = get_object_or_404(UserProfile,
                screen_name=self.kwargs['screen_name'],
                )
        user = self.request.user
        op_user = user if not user.is_anonymous() else None

        is_my_tutor = Ring.objects.filter(
                tutor=profile_user, student=op_user, status='accepted'
                ).exists()
        is_my_student = Ring.objects.filter(
                student=profile_user, tutor=op_user, status='accepted'
                ).exists()
        ring = Ring.objects.filter(
                tutor=profile_user, student=op_user
                )
        ring = ring[0] if ring.exists() else None 
        is_follower = Ring.objects.filter(
                student=profile_user, tutor=op_user, follow=True
                ).exists()

        context.update({
            'is_my_tutor': is_my_tutor,
            'is_my_student': is_my_student,
            'ring': ring,
            'nav_me': 'correction',
            'is_follower': is_follower,
            'profile_user': profile_user,
            })
        return context


class AskLanguageList(ListView, LangExtraContext):
    model = Ask
    context_object_name = 'ask_list'
    template_name='lang/ask/language.html'
    paginate_by = 10

    def get_queryset(self):
        if self.kwargs.has_key('language'):
            language = get_object_or_404(Language,
                    name=self.kwargs['language'],
                    )
            queryset = Ask.objects.filter(language=language, is_published=True)
        else:
            queryset = Ask.objects.filter(is_published=True)
        return queryset

    def get_context_data(self, *args, **kwargs):
        context = super(AskLanguageList, self).get_context_data(
                *args, **kwargs)
        user = self.request.user
        today = date.today()
        previous_week_year = today.year if today.month != 1 else today.year - 1
        previous_week = today.isocalendar()[1] - 1

        w = Week(previous_week_year, previous_week)
        if self.kwargs.has_key('language'):
          language = Language.objects.get(name=self.kwargs['language'])
        else:
          language = None

        try:
            rank_period = RankPeriod.objects.get(
                    language=language,
                    week=previous_week, year=previous_week_year
                    )
            ranks = Rank.objects.filter(period=rank_period).distinct()[:3]
        except ObjectDoesNotExist:
            ranks = []

        mystudents = []
        if user.is_authenticated():
            mystudents = user.my_students().values_list('student', flat=True)

        context.update({
            'language': language,
            'tutors': ranks,
            'mystudents': mystudents,
            })
        return context


class AskMyStudentList(ListView, LangExtraContext):
    model = Ask
    context_object_name = 'ask_list'
    template_name='lang/ask/language.html'
    paginate_by = 10

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AskMyStudentList, self).dispatch(
                request, *args, **kwargs)

    def get_queryset(self):
        user = self.request.user
        queryset = Ask.objects.filter(
                is_published=True,
                user__in=user.my_students().values_list('student', flat=True))
        return queryset

    def get_context_data(self, *args, **kwargs):
        context = super(AskMyStudentList, self).get_context_data(
                *args, **kwargs)

        user = self.request.user
        today = date.today()
        previous_week_year = today.year if today.month != 1 else today.year - 1
        previous_week = today.isocalendar()[1] - 1

        w = Week(previous_week_year, previous_week)
        language = user.get_native_language()

        try:
            rank_period = RankPeriod.objects.get(
                    language=language,
                    week=previous_week, year=previous_week_year
                    )
            ranks = Rank.objects.filter(period=rank_period).distinct()[:3]
        except ObjectDoesNotExist:
            ranks = []

        mystudents = user.my_students().values_list('student', flat=True)

        context.update({
            'language': 'mystudents',
            'mystudents': mystudents,
            'tutors': ranks,
            })
        return context


class AskPost(CreateView, LangExtraContext, MessageMixin):
    model = Ask
    form_class = AskForm
    template_name='lang/ask/post.html'
    success_message = "Post Submit Successfully"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AskPost, self).dispatch(
                request, *args, **kwargs)

    def get_form_kwargs(self):
         kwargs = super(AskPost, self).get_form_kwargs()
         kwargs.update({
             'user' : self.request.user
         })
         return kwargs

    def get_success_url(self):
        redirect_url = reverse('ask_update', args=(self.object.id,))
        redirect_url += "?draft=true"
        return redirect_url

    def form_invalid(self, form):
        if form['context'].value() or form['title'].value():
            return super(AskPost, self).form_invalid(form)

        language = self.request.user.get_learning_language().name
        return HttpResponseRedirect(
                reverse('ask_language_list', args=(language,)))

    @transaction.commit_on_success
    def form_valid(self, form):
        ask = form.save()

        user = self.request.user
        if form.cleaned_data.get('title'):
            Sentence.objects.create(
                ask=ask, type='title', user=user,
                context=form.cleaned_data.get('title'))

        import re
        lines = re.split(ur"[\n\r]+", ask.context)
        for line in lines:
            if not line: continue
            pattern = re.compile(
                    #ur'\w.*?[\.!?]+', re.U | re.M | re.DOTALL)
                    ur'(\w.*?[\.!?。]+|\w.*?$)', re.U | re.M | re.DOTALL)
            splited_context = re.findall(pattern, line)
            if splited_context:
                for context in splited_context:
                    if not context: continue
                    Sentence.objects.create(
                            ask=ask, context=context, user=user)
            else:
                sentence = Sentence.objects.create(
                        ask=ask, context=line, user=user)
        ask.user = user
        ask.language = user.get_learning_language()
        ask.save()

        if ask.topic:
            ask.topic.reader.add(user)

        self.object = ask
        messages.success(self.request, self.success_message)
        return HttpResponseRedirect(self.get_success_url())


class CommentForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = ('context', )


class AskView(FormView, DetailView, LangExtraContext, MessageMixin):
    model = Ask
    form_class = CommentForm
    context_object_name = 'ask'
    template_name='lang/ask/detail.html'
    success_message = "Correction Submit Successfully"

    def dispatch(self, request, *args, **kwargs):
        object = self.get_object()
        user = request.user

        if request.user != object.user and not object.is_published:
            raise Http404
            return HttpResponseRedirect(reverse('ask_post_list',
                args=(user.screen_name,)))

        correction_queryset = object.correction_set
        if not self.kwargs.has_key('corrector_name'):
            if request.user == object.user and correction_queryset.exists():
                correction = correction_queryset.all()[0]
                return HttpResponseRedirect(reverse('ask_correction_view',
                    args=(object.id, correction.user.screen_name)))
            elif correction_queryset.exists():
                if user.is_authenticated() and correction_queryset.filter(user=user).exists():
                    return HttpResponseRedirect(reverse('ask_correction_view',
                        args=(object.id, user.screen_name)))

        return super(AskView, self).dispatch(
                request, *args, **kwargs)

    def get_object(self):
        ask = get_object_or_404(Ask, id=self.kwargs['pk'])
        user = self.request.user

        if user.is_authenticated():
            notifications =  Notification.objects.filter(
                    receiver=user, post=ask, type__in=['correction', 'reply'])
            try:
                notifications.get(type='reply').off()
            except ObjectDoesNotExist:
                pass

        if user == ask.user:
            try:
                notifications.get(type='correction').off()
            except ObjectDoesNotExist:
                pass
        return ask

    def get_context_data(self, *args, **kwargs):
        context = super(AskView, self).get_context_data(**kwargs)
        if self.kwargs.has_key('corrector_name'):
            context['corrector']=self.kwargs['corrector_name']
        return context

    def post(self, request, *args, **kwargs):
        user = self.request.user
        ask = self.get_object()
        values = self.request.POST
        corrected_sentence_count = 0

        if not user.is_authenticated():
            return HttpResponseRedirect(reverse('login'))
        is_author = False if ask.user != user else True

        # create sentence of correction
        correction = Correction.objects.create(ask=ask, user=user)

        # create rating
        grammer = values.get('grammer', None)
        spelling = values.get('spelling', None)
        difficulty = values.get('difficulty', None)
        comment = values.get('comment', None)

        if grammer or spelling or difficulty:
            rating = Rating.objects.create(
                    grammer=(grammer if grammer else 0),
                    spelling=(spelling if spelling else 0),
                    difficulty=(difficulty if difficulty else 0),
                    comment=(comment if comment else None)
                    )
        else:
            rating = Rating.objects.create(
                    grammer=(grammer if grammer else None),
                    spelling=(spelling if spelling else None),
                    difficulty=(difficulty if difficulty else None),
                    comment=(comment if comment else None)
                    )
        correction.rating = rating
        correction.save()

        sentences = ask.sentence_set.all()
        for sentence in sentences:
            value = ""
            html_context = ""
            parent_sentence_id = str(sentence.id)
            if values.has_key("s_" + parent_sentence_id):
                value = values["s_" + parent_sentence_id]

                dmp = diff_match_patch()
                dmp.Diff_EditCost=1
                d = dmp.diff_main(sentence.context, value)
                dmp.diff_cleanupEfficiency(d)
                html_context = dmp.diff_prettyHtml(d)

            corrected_sentence = None
            if value:
                corrected_sentence = Sentence.objects.create(
                        user=user, parent=sentence, correction=correction,
                        context=value, html_context=html_context,
                        type=sentence.type,
                        )
                corrected_sentence_count += 1

            if values.has_key("c_" + parent_sentence_id) and \
                    values["c_" + parent_sentence_id]:
                if not corrected_sentence:
                    corrected_sentence = Sentence.objects.create(
                        user=user, parent=sentence, correction=correction,
                        context=value, html_context=html_context,
                        type=sentence.type,
                        )

                comment = Comment.objects.create(
                        parent=corrected_sentence, user=user,
                        context=values['c_' + parent_sentence_id],
                        )

        point_log, grade = user.point.acquire_point('correction', ask, 
            sentences=corrected_sentence_count
            )
        messages.success(self.request, self.success_message,
                extra_tags=point_log.point if point_log else None)

        # send notification and email to post author
        if not is_author:
            notification, created = Notification.objects.get_or_create(
                    type='correction', post=ask, receiver=ask.user)
            notification.sender.add(user)
            notification.is_new=True
            notification.save()
            ask.user.set_level()
            user.set_level()

            try:
            	mailman = LingRingMailman()
            	mailman.correction(
                        user.screen_name, correction.sentence_set.all(), ask)
            except:
                pass

        return HttpResponseRedirect('%s%s' %(
                    reverse('ask_correction_view', args=(ask.id, correction.user.screen_name)),
                    '?grade=' + grade if grade else ''
                ))


class AskBump(UpdateView, JSONResponseMixin):
    model = Ask
    context_object_name = 'ask'

    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AskBump, self).dispatch(
                request, *args, **kwargs)

    def render_to_json_response(self, context, **response_kwargs):
        data = json.dumps(context)
        response_kwargs['content_type'] = 'application/json'
        return HttpResponse(data, **response_kwargs)

    @transaction.commit_on_success
    def post(self, request, *args, **kwargs):
        user = self.request.user
        user_point =  user.point.current_point()
        context = { 'status': 'fail' }

        if user_point < POINT_VALUES['bumpup']:
            context.update({
                'message': ugettext_lazy('Not enough point for BumpUp').encode('utf-8')
                })
            return JSONResponseMixin.render_to_response(self, context)

        ask = self.get_object()
        log = user.point.lose_point('bumpup', ask)

        if not log:
            context.update({
                'message': ugettext_lazy('Already used BumpUp to this post').encode('utf-8')
                })
            return JSONResponseMixin.render_to_response(self, context)

        ask.save()

        message = ugettext_lazy(
                'Your post is moved to the top. You can find the post in the Home\'s learning language. You have %(left_point)s points now.'
                ) % { 'left_point': user.point.current_point() }
        context.update({
            'status': 'success',
            'ask_id': ask.id,
            'message': message.encode('utf-8'),
            'used_point': POINT_VALUES['bumpup'],
            })
        return JSONResponseMixin.render_to_response(self, context)


class AskUpdate(UpdateView, LangExtraContext, MessageMixin):
    model = Ask
    context_object_name = 'ask'
    template_name='lang/ask/edit.html'
    success_message = "Post Update Successfully"

    def get(self, request, *args, **kwargs):
        ask = self.get_object()
        if self.request.user != ask.user:
            return HttpResponseRedirect(
                    reverse('ask_view', args=(ask.id, )))
        return super(AskUpdate, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        ask = self.get_object()
        user = self.request.user
        if not user.is_authenticated() or ask.user != user:
            return HttpResponseRedirect(reverse('login'))

        values = self.request.POST
        native_context_flag = False
        sentences = {}

        #   post parameter to dictionary
        for key, value in values.iteritems():
            if not (key.startswith("l_") or key.startswith("n_") or \
                    key.startswith("tl_") or key.startswith("tn_")):
                continue
            posttype, parent_id = key.split('_')
            if not sentences.has_key(parent_id):
                sentences[parent_id] = {}
            sentences[parent_id][posttype] = value

        #   sentence update
        ask.context = ""
        ask.original_context = ""
        for parent_id in sorted(sentences.iterkeys()):
            contexts = sentences[parent_id]
            is_title = False
            if contexts.has_key('l'):
                learn_context = contexts['l']
            elif contexts.has_key('tl'):
                learn_context = contexts['tl']
                is_title = True
            else:
                learn_context = ""

            if contexts.has_key('n'):
                native_context = contexts['n']
                if native_context:
                    native_context_flag = True 
            elif contexts.has_key('tn'):
                native_context = contexts['tn']
                if native_context:
                    native_context_flag = True 
                is_title = True
            else:
                native_context = ""

            try:
                sentence = Sentence.objects.get(id=parent_id)
                if not learn_context:
                    if is_title:
                        ask.title = None
                        ask.save(bump_time=False)
                    sentence.delete()
                    continue

                sentence.context = learn_context
                sentence.native_context = native_context
                sentence.save()
                if not is_title:
                    ask.context += "\n" + learn_context \
                            if ask.context else learn_context
                    ask.original_context += "\n" + native_context \
                            if ask.original_context else native_context
            except ObjectDoesNotExist:
                pass
            ask.save(bump_time=False)

        if not ask.sentences:
            ask.delete()
            ask.user.point.lose_point('delete_post', ask)
            messages.success(self.request, "Deleted Successfully")
            return HttpResponseRedirect(
                    reverse('ask_post_list', args=(user.screen_name, )))

        grade = ''
        points = 0
        if native_context_flag:
            point_log, grade = ask.user.point.acquire_point('add_native', ask)
            points += point_log.point if point_log else 0
                
        if not ask.is_published:
            ask.is_published=True
            ask.save()

            point_log, grade = ask.user.point.acquire_point('add_post', ask)
            points += point_log.point
            ask.user.set_level()

            if ask.user.my_tutors().exists():
                try:
                    mailman = LingRingMailman()
                    mailman.posted_by_student(ask)
                except:
                    pass

        messages.success(self.request, self.success_message,
                extra_tags=points if points else None)

        return HttpResponseRedirect('%s%s' %(
                    reverse('ask_view', args=(ask.id,)),
                    '?success=true' + ('&grade=' + grade if grade else '')
                ))


class AskDelete(MessageMixin, DeleteView):
    model = Ask
    success_message = "Deleted Successfully"

    def post(self, request, *args, **kwargs):
        ask = self.get_object()
        ask.user.point.lose_point('delete_post', ask)
        return super(AskDelete, self).post(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('ask_post_list', args=(self.request.user.screen_name,))



class CorrectionForm(forms.ModelForm):

    class Meta:
        model = Sentence
        exclude = ('user', 'ip_addr', 'ask', 'native_context', )


class CorrectionCreate(CreateView, LangExtraContext, MessageMixin):
    model = Sentence
    form_class = CorrectionForm
    template_name='lang/ask/detail.html'
    success_message = "Correction Submit Successfully"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CorrectionCreate, self).dispatch(
                request, *args, **kwargs)

    def form_invalid(self, form):
        messages.set_level(self.request, messages.ERROR)
        messages.error(self.request, "%s" % form.errors)
        return HttpResponseRedirect(
                reverse('ask_view', args=(self.kwargs['pk'], )))

    def form_valid(self, form):
        user = self.request.user

        correction = form.save(commit=False)
        correction.user = user
        correction.ip_addr = self.request.META.get('REMOTE_ADDR')

        dmp = diff_match_patch()
        dmp.Diff_EditCost=1
        d = dmp.diff_main(correction.parent.context, correction.context)
        dmp.diff_cleanupEfficiency(d)
        html_context = dmp.diff_prettyHtml(d)
        correction.html_context = html_context
        correction.save()

        messages.success(self.request, self.success_message)

        return HttpResponseRedirect(
                reverse('ask_view', args=(self.kwargs['pk'],)))
