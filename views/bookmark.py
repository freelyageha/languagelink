# -*- coding: utf-8 -*-
from datetime import datetime

from django import forms
from django.shortcuts import get_object_or_404
from django.views.generic import FormView, UpdateView, ListView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.db import transaction
from django.db.models import Q
from django.utils.translation import ugettext
from django.core.exceptions import ObjectDoesNotExist

from lang.models import Bookmark, Ask, Sentence, UserProfile
from lang.views import JSONResponseMixin, LangExtraContext, MessageMixin


class BookmarkList(ListView, LangExtraContext, MessageMixin):
    model = Bookmark
    context_object_name = 'bookmarks'
    template_name='lang/bookmark/list.html'
    paginate_by = 10
    error_message = ugettext("You does not have permission for read notes")

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        user = request.user
        profile_user = get_object_or_404(UserProfile,
                screen_name=self.kwargs['screen_name'],
                )
        if user != profile_user:
            messages.error(self.request, "%s" % self.error_message)
            return HttpResponseRedirect(reverse('ask_post_list',
                args=(profile_user.screen_name,)))
        return super(BookmarkList, self).dispatch(
                request, *args, **kwargs)

    def get_queryset(self):
        user = get_object_or_404(UserProfile,
                screen_name=self.kwargs['screen_name'],
                )
        queryset = Bookmark.objects.filter(user=user)
        return queryset

    def get_context_data(self, *args, **kwargs):
        context = super(BookmarkList, self).get_context_data(
                *args, **kwargs)
        context.update({
            'nav_me': 'bookmarks',
            })
        return context


class BookmarkForm(forms.ModelForm):

    class Meta:
        model = Bookmark
        exclude = ('user', 'ip_addr',)


class BookmarkCreateOrDelete(JSONResponseMixin, FormView):
    model = Bookmark
    form_class = BookmarkForm

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(BookmarkCreateOrDelete, self).dispatch(
                request, *args, **kwargs)

    def form_invalid(self, form):
        context = {}
        context.update({
            'status': 'error'
            })
        if self.request.is_ajax():
            return JSONResponseMixin.render_to_response(
                self, context)
        else:
            return super(BookmarkCreateOrDelete, self).form_invalid(form)

    def form_valid(self, form):
        user = self.request.user
        sentence_id = self.request.POST.get('sentence', '')
        context = {}

        sentence = Sentence.objects.get(id=sentence_id)
        bookmark, created = Bookmark.objects.get_or_create(
                user=user, sentence=sentence)
        if not created:
            bookmark.delete()
            context.update({ 'bookmarked': '' })
        else:
            context.update({ 'bookmarked': 'bookmarked' })
        return JSONResponseMixin.render_to_response(self, context)
