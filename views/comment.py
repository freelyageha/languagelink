# -*- coding: utf-8 -*-
from datetime import datetime

from django import forms
from django.shortcuts import get_object_or_404
from django.views.generic import FormView, UpdateView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.db import transaction
from django.db.models import Q

from lang.models import Comment, Sentence, Notification, OverallComment, Ask, UserProfile, Correction
from lang.views import JSONResponseMixin
from lang.mail import LingRingMailman

class CommentForm(forms.ModelForm):

    class Meta:
        model = Comment
        exclude = ('user', 'ip_addr', 'parent')


class CommentCreate(JSONResponseMixin, FormView):
    model = Comment
    form_class = CommentForm

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CommentCreate, self).dispatch(
                request, *args, **kwargs)

    def form_invalid(self, form):
        context = {}
        context.update({
            'status': 'error'
            })
        if self.request.is_ajax():
            return JSONResponseMixin.render_to_response(
                self, context)
        else:
            return super(CommentCreate, self).form_invalid(form)

    def form_valid(self, form):
        user = self.request.user
        pid = self.request.POST.get('parent', '')
        content = self.request.POST.get('context', '')

        correction = get_object_or_404(Sentence, id=pid)
        ask = correction.parent.ask

        comment = Comment.objects.create(
                user=user, parent=correction, context=content)

        if user == correction.user:
            receiver = ask.user
            _type = 'correction'
        else:
            receiver = correction.user
            _type = 'reply'

        notification, created = Notification.objects.get_or_create(
                type=_type , post=ask, receiver=receiver)
        notification.sender.add(user)
        notification.is_new=True
        notification.save()

        ask.user.set_level()
        user.set_level()

        try:
            mailman = LingRingMailman()
            mailman.reply(user.screen_name, content, ask, [receiver])
        except:
            pass

        context = {}
        context.update({
            'status': 'success',
            'comment': comment.context,
            'comment_id': comment.id,
            'ctime': datetime.now().strftime("%m-%d %H:%M")
            })
        return JSONResponseMixin.render_to_response(self, context)


class CommentUpdate(JSONResponseMixin, UpdateView):
    model = Comment
    form_class = CommentForm

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CommentUpdate, self).dispatch(
                request, *args, **kwargs)

    def form_invalid(self, form):
        context = {}
        if not form.clean():
            comment = self.get_object()
            comment.delete()
            context.update({
                'status': 'success',
                'deleted': True
                })
        else:
            context.update({
                'status': 'error'
                })

        return JSONResponseMixin.render_to_response(
            self, context)

    def form_valid(self, form):
        comment = form.save()
        context = {}
        context.update({
            'status': 'success',
            'comment': comment.context,
            'value': comment.context,
            })
        return JSONResponseMixin.render_to_response(self, context)



class OverallCommentForm(forms.ModelForm):

    class Meta:
        model = OverallComment
        exclude = ('user', 'ip_addr', 'correction')


class OverallCommentCreate(JSONResponseMixin, FormView):
    model = OverallComment
    form_class = OverallCommentForm

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(OverallCommentCreate, self).dispatch(
                request, *args, **kwargs)

    def form_invalid(self, form):
        context = {}
        context.update({
            'status': 'error'
            })
        if self.request.is_ajax():
            return JSONResponseMixin.render_to_response(
                self, context)
        else:
            return super(OverallCommentCreate, self).form_invalid(form)

    @transaction.commit_on_success
    def form_valid(self, form):
        user = self.request.user
        correction_id = self.request.POST.get('correction', '')
        correction = Correction.objects.get(id=correction_id)
        ask = correction.ask

        # create blank comment for first comment, It is a legacy
        comment = form.save()
        comment.user = user
        comment.correction = correction
        comment.save()

        receiver_ids = set(correction.overallcomment_set.filter(~Q(user=user)).order_by('user').values_list('user', flat=True).distinct())

        if user == ask.user:
            receiver_ids.add(correction.user.id)
        elif user == correction.user:
            receiver_ids.add(ask.user.id)
        else:
            receiver_ids.add(correction.user.id)
            receiver_ids.add(ask.user.id)
        receivers = UserProfile.objects.filter(id__in=receiver_ids)

        for receiver in receivers:
            _type = "reply"

            notification, created = Notification.objects.get_or_create(
                type=_type, post=ask, receiver=receiver)
            notification.sender.add(user)
            notification.is_new=True
            notification.save()

            try:
                mailman = LingRingMailman()
                mailman.reply(
                        user.screen_name, comment.context, ask, receivers)
            except:
                pass


        ask.user.set_level()
        user.set_level()

        if self.request.is_ajax():
            context = {}
            context.update({
                'status': 'success',
                'comment_id': comment.id,
                'comment': comment.context,
                'ctime': datetime.now().strftime("%m-%d %H:%M")
                })
            return JSONResponseMixin.render_to_response(self, context)
        else:
            return HttpResponseRedirect(
                    reverse('ask_view', args=(ask.id,)))


class OverallCommentUpdate(JSONResponseMixin, UpdateView):
    model = OverallComment
    form_class = OverallCommentForm

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(OverallCommentUpdate, self).dispatch(
                request, *args, **kwargs)

    def form_invalid(self, form):
        context = {}
        if not form.clean():
            comment = self.get_object()
            comment.delete()
            context.update({
                'status': 'success',
                'deleted': True
                })
        else:
            context.update({
                'status': 'error'
                })

        return JSONResponseMixin.render_to_response(
            self, context)

    def form_valid(self, form):
        comment = form.save()
        context = {}
        context.update({
            'status': 'success',
            'comment': comment.context,
            'value': comment.context,
            })
        return JSONResponseMixin.render_to_response(self, context)
