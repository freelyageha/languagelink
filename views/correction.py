# -*- coding: utf-8 -*-
from datetime import datetime
from diff_match_patch import *

from django import forms
from django.conf import settings
from django.shortcuts import get_object_or_404
from django.views.generic import FormView, UpdateView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.db import transaction

from lang.models import Correction, Sentence
from lang.views import JSONResponseMixin
from lang.mail import LingRingMailman


class SentenceForm(forms.ModelForm):

    class Meta:
        model = Sentence
        exclude = ('user', 'ip_addr', 'type', 'ask', 'correction',
                'type', 'parent', 'native_context', 'tags')


class SentenceUpdate(JSONResponseMixin, UpdateView):
    model = Sentence
    form_class = SentenceForm

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(SentenceUpdate, self).dispatch(
                request, *args, **kwargs)

    def form_invalid(self, form):
        context = {}
        context.update({
            'status': 'error'
            })
        if self.request.is_ajax():
            return JSONResponseMixin.render_to_response(
                self, context)
        else:
            return super(SentenceUpdate, self).form_invalid(form)

    def form_valid(self, form):
        sentence = form.save(commit=False)

        dmp = diff_match_patch()
        dmp.Diff_EditCost=1
        d = dmp.diff_main(sentence.parent.context, sentence.context)
        dmp.diff_cleanupEfficiency(d)
        html_context = dmp.diff_prettyHtml(d)
        sentence.html_context = html_context
        sentence.save()

        context = {}
        if sentence.context == '[Perfect]':
            comment = \
                '<img src="' + settings.STATIC_URL + 'image/ic_perfect.png' + '" />'
        elif sentence.context == '[Do-not-understand]':
            comment = \
                '<img src="' + settings.STATIC_URL + 'image/ic_dontunderstand.png' + '" />'
        else:
            comment = html_context

        context.update({
            'status': 'success',
            'comment': comment,
            'value': sentence.context,
            })
        return JSONResponseMixin.render_to_response(self, context)
