#-*- coding: utf-8 -*-

import json

from datetime import datetime

from django import forms
from django.db import transaction
from django.db.models import Q, F, Max, Count, Sum, Avg
from django.shortcuts import render_to_response, get_object_or_404
from django.views.generic import ListView, DetailView, TemplateView, View
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.utils.translation import ugettext_lazy
from django.utils.decorators import method_decorator
from django.core import serializers

from lang.models import PointLog, Point, UserProfile
from lang.views import LangExtraContext, JSONResponseMixin
from lang.utils import merge_zero_list_to_single_list


class Growth(TemplateView, LangExtraContext):
    template_name = 'lang/growth/view.html'

    def get_context_data(self, *args, **kwargs):
        context = super(Growth, self).get_context_data(
                *args, **kwargs)
        profile_user = get_object_or_404(UserProfile,
                screen_name=self.kwargs['screen_name'],
                )
        context.update({
            'profile_user': profile_user,
            'nav_me': 'growth',
            })
        return context


class ScoreGrowth(JSONResponseMixin, DetailView):
    model = UserProfile

    def get(self, *args, **kwargs):
        user = self.get_object()
        asks = user.ask_set.filter(
                    is_published=True,
                    correction__ctime__gt=datetime(2014, 5, 24, 0, 0, 0),
                ).annotate(
                    grammer_score=Avg('correction__rating__grammer'),
                    spelling_score=Avg('correction__rating__spelling'),
                    difficulty_score=Avg('correction__rating__difficulty')
                    ).order_by('-ctime')[:10]

        grammer_score =  list(asks.values_list('grammer_score', flat=True))
        spelling_score = list(asks.values_list('spelling_score', flat=True))
        difficulty_score = list(asks.values_list('difficulty_score', flat=True))
        created_times =  asks.values_list('ctime', flat=True)
        overall_score = [
                (int(x or 0) + int(y or 0) + int(z or 0)) / 3 for x, y, z in zip(grammer_score, spelling_score, difficulty_score)
                ]
        categories = map(lambda x: x.strftime("%m.%d"), created_times)
        overall_score.reverse()
        categories.reverse()

        level = {
                1: 'Beginner',
                2: 'Beginner',
                3: 'Intermediate',
                4: 'Expert',
                5: 'Expert',
                }

        avg_score = sum(overall_score) / float(len(overall_score))
        subtitle = "Score: %s / Level : %s" % (avg_score, level[int(avg_score)])

        context = {
                'title': {
                    'text': 'Average Score of Recent 10 Posts',
                    },
                'subtitle': {
                    'text': subtitle,
                    },
                'xAxis': {
                    'categories': categories,
                    },
                'yAxis': {
                    'max': 5,
                    'title': {
                        'text': 'Score'
                        }
                    },
                'series': [
                    {
                        'data': merge_zero_list_to_single_list(grammer_score),
                        'name': 'Grammer',
                        },
                    {
                        'data': merge_zero_list_to_single_list(spelling_score),
                        'name': 'Spelling',
                        },
                    {
                        'data': merge_zero_list_to_single_list(difficulty_score),
                        'name': 'Difficulty',
                        },
                    {
                        'data': merge_zero_list_to_single_list(overall_score),
                        'name': 'Overall',
                        },
                    ]
                }

        return JSONResponseMixin.render_to_response(self, context)

class EventGrowth(JSONResponseMixin, DetailView):
    model = UserProfile

    def get(self, *args, **kwargs):
        user = self.get_object()

        asks = user.ask_set.filter(
                is_published=True).annotate(
                corrections=Count('correction')
                )
        created_times = asks.values_list('ctime', flat=True)
        context = map(lambda x: {'date': x.strftime("%Y-%m-%d") }, created_times)
        return JSONResponseMixin.render_to_response(self, context)



