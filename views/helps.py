from django.views.generic.base import TemplateView
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext_lazy

from lang.views import LangExtraContext
from lang.models import UserMeta

class HelpView(TemplateView, LangExtraContext):

    def get_template_names(self):
        return "lang/help/index.html"


class LegacyHelpView(TemplateView, LangExtraContext):

    def get_template_names(self):
        help_index = int(self.kwargs['pk'])
        return "lang/help/h" + str(help_index) + ".html"

    def get_context_data(self, **kwargs):
        context = super(LegacyHelpView, self).get_context_data(**kwargs)
        help_index = int(self.kwargs['pk'])
        context.update({
            'current': help_index,
            'previous': help_index - 1 if help_index > 1 else None,
            'next': help_index + 1 if help_index < 7 else None,
            })
        return context


class StaticHelpView(TemplateView, LangExtraContext):

    def get_template_names(self):
        help_slug = self.kwargs['slug']
        return "lang/help/" + help_slug + ".html"

    def get(self, *args, **kwargs):
        slug = kwargs.get('slug', None)
        user = self.request.user
        if slug == 'how-do-you-think-of-lingring' and user.is_authenticated():
            try:
                usermeta = user.usermeta
                usermeta.view_lingring_think = True
                usermeta.save()
            except ObjectDoesNotExist:
                UserMeta.objects.create(user=user, view_lingring_think=True)
        return super(StaticHelpView, self).get(*args, **kwargs)
