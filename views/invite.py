# -*- coding: utf-8 -*-
from datetime import datetime

from django import forms
from django.shortcuts import get_object_or_404
from django.views.generic import FormView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.db import transaction

from lang.models import UserProfile, Invite
from lang.views import JSONResponseMixin


class InviteForm(forms.ModelForm):

    class Meta:
        model = Invite
        exclude = ('user', 'ip_addr', 'to',)


class InviteCreate(JSONResponseMixin, FormView):
    model = Invite
    form_class = InviteForm

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(InviteCreate, self).dispatch(
                request, *args, **kwargs)

    def form_invalid(self, form):
        context = {}
        context.update({
            'status': 'error'
            })
        if self.request.is_ajax():
            return JSONResponseMixin.render_to_response(
                self, context)
        else:
            return super(InviteCreate, self).form_invalid(form)

    @transaction.commit_on_success
    def form_valid(self, form):
        user = self.request.user
        to = self.request.POST.getlist('to[]', '')

        for u in to:
            invite, created = Invite.objects.get_or_create(user=user, to=u)
        context = {}
        context.update({
            'status': 'success',
            })
        return JSONResponseMixin.render_to_response(self, context)
