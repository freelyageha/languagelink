import time
from django.views.generic.base import TemplateView
from django.conf import settings
from django.views.generic import FormView, UpdateView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

from lang.models import Notification
from lang.views import JSONResponseMixin

from redis import Redis

class Pipe(object):

    def __init__(self):
        self.conn = Redis(
                host = '127.0.0.1', port = 6379, db = settings.REDIS_DB)

    def push(self, name, key, message=''):
        self.conn.rpush(name + ':' + str(key), message)

    def pop(self, name, key):
        self.conn.rpop(name + ':' + str(key))

    def count(self, name):
        return self.conn.keys(name + ':*')

    def list(self, name, key):
        _list = self.conn.lrange(name + ':' + key, 0, -1)
        return _list

    def clear(self, name, key):
        self.conn.delete(name + ':' + key)


class NotificationView(JSONResponseMixin, TemplateView):

    template_name = "notification.html"

    def render_to_response(self, context):
        _filter = ''
        if self.request.GET.has_key('filter'):
            _filter = self.request.GET['filter']

        user = self.request.user
        context = {}
        pipe = Pipe()

        if _filter in ['post', 'chatroom']:
            chatrooms = pipe.count(str(user.id) + ":chatrooms")
            corrections = pipe.count(str(user.id) + ":corrections")
            context.update({ 'chatrooms': len(chatrooms) })
            context.update({ 'corrections': len(corrections) })

            if _filter == 'post':
                context.update({ 'correction_list': corrections })
            elif _filter == 'chatroom':
                context.update({ 'chatroom_list': chatrooms })

        elif _filter == 'talking':
            for index in range(14):
              room_id = self.request.GET['room_id']
              messages = pipe.list(str(user.id) + ":chatrooms", room_id)
              if messages:
                context.update({ 'messages': messages })
                pipe.clear(str(user.id) + ":chatrooms", room_id)
                break
              else:
                time.sleep(2)

        else:
            chatrooms = pipe.count(str(user.id) + ":chatrooms")
            corrections = pipe.count(str(user.id) + ":corrections")
            context.update({ 'chatrooms': len(chatrooms) })
            context.update({ 'corrections': len(corrections) })

        return JSONResponseMixin.render_to_response(self, context)



class NotificationRead(JSONResponseMixin, UpdateView):
    model = Notification

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(NotificationRead, self).dispatch(
                request, *args, **kwargs)

    def post(self, *args, **kwargs):
        notification = self.get_object()
        notification.is_new=False
        notification.save()

        context = { 'status': 'success' }
        return JSONResponseMixin.render_to_response(self, context)

