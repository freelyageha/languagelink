import json

from datetime import datetime

from django import forms
from django.db import transaction
from django.db.models import Q
from django.shortcuts import render_to_response, get_object_or_404
from django.views.generic import ListView
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.utils.translation import ugettext_lazy
from django.utils.decorators import method_decorator

from lang.models import PointLog, Point, UserProfile
from lang.views import LangExtraContext


class PointList(ListView, LangExtraContext):
    model = PointLog
    context_object_name = 'pointlogs'
    template_name = 'lang/point/list.html'
    paginate_by = 10

    def get_queryset(self):
        profile_user = get_object_or_404(UserProfile,
                screen_name=self.kwargs['screen_name'],
                )
        return PointLog.objects.filter(parent__user=profile_user)

