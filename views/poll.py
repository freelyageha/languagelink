import json

from datetime import datetime

from django import forms
from django.db import transaction
from django.db.models import Q
from django.shortcuts import render_to_response, get_object_or_404
from django.views.generic import FormView
from django.views.generic import CreateView, UpdateView, \
        DeleteView, ListView, DetailView
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.core.exceptions import ValidationError
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.utils.translation import ugettext_lazy
from django.utils.decorators import method_decorator

from lang.models import Poll, Event, UserProfile
from lang.choices import HELPFUL_CHOICE, WILL_INVITE_CHOICE, REWARD_CHOICE
from lang.views import LangExtraContext, MessageMixin, JSONResponseMixin


class PollForm(forms.Form):
    is_helpful = forms.ChoiceField(
            label=ugettext_lazy('Is Ling Ring helpful to learn a language?'),
            widget=forms.RadioSelect, choices=HELPFUL_CHOICE)
    will_invite = forms.ChoiceField(
            label=ugettext_lazy('Are you willing to invite your friend to Ling Ring?'),
            widget=forms.RadioSelect, choices=WILL_INVITE_CHOICE)
    reward = forms.MultipleChoiceField(
            label=ugettext_lazy('What kinds of reward do you want in return for your friend invitation?'),
            required=False,
            widget=forms.CheckboxSelectMultiple, choices=REWARD_CHOICE)
    know_lingring = forms.TypedChoiceField(
            label= ugettext_lazy('Do you know Ling Ring has a facebook page?'),
            coerce=lambda x: x == 'True',
            choices=((False, 'No'), (True, 'Yes')),
            widget=forms.RadioSelect)
    suggest = forms.CharField(
            label=ugettext_lazy('Tell us any suggestion or complaint if you have.'),
            widget=forms.Textarea, required=False)


    def save(self, user, *args, **kwargs):
        poll, created = Poll.objects.get_or_create(event_id=1, user=user)
        if created:
            import json
            poll.context = json.dumps(self.cleaned_data)
            poll.save()
        return poll


class PollCreate(FormView, LangExtraContext, MessageMixin):
    form_class = PollForm
    template_name='lang/poll/create.html'
    success_message = "Survey Submit Successfully. Thank You!"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        user = self.request.user
        event_id = kwargs['event_id']
        if not Event.objects.filter(id=event_id, active=True).exists():
            return HttpResponseRedirect(reverse(
                    'ask_post_list', args=(self.request.user.screen_name,)))
        poll = Poll.objects.filter(user=user, event_id=event_id).exists()
        if poll:
            return HttpResponseRedirect(reverse(
                    'ask_post_list', args=(self.request.user.screen_name,)))
        return super(PollCreate, self).dispatch(
                request, *args, **kwargs)

    def get_success_url(self):
        return reverse('ask_post_list', args=(self.request.user.screen_name,))

    @transaction.commit_on_success
    def form_valid(self, form):
        poll = form.save(self.request.user)
        self.request.user.point.acquire_point('survey')
        messages.success(self.request, self.success_message,)
        return HttpResponseRedirect(self.get_success_url())

