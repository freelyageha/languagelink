from django.views.generic.base import TemplateView
from django.utils.translation import ugettext_lazy

from lang.views import LangExtraContext


class PrivacyView(TemplateView, LangExtraContext):
    template_name = "lang/legal/privacy.html"
