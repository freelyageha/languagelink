import json

from datetime import datetime

from django import forms
from django.db import transaction
from django.db.models import Q
from django.shortcuts import render_to_response, get_object_or_404
from django.views.generic import UpdateView, ListView, DetailView
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.core.exceptions import ValidationError
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.utils.translation import ugettext_lazy
from django.utils.decorators import method_decorator

from lang.models import QuizBook, Quiz, UserProfile, Ring
from lang.views import LangExtraContext


class QuizBookList(ListView, LangExtraContext):
    model = QuizBook
    context_object_name = 'quizbooks'
    template_name = 'lang/quizbook/list.html'
    paginate_by = 10

    def get_queryset(self):
        today = datetime.today().strftime("%Y-%m-%d")
        profile_user = get_object_or_404(UserProfile,
                screen_name=self.kwargs['screen_name'],
                )
        return QuizBook.objects.filter(
                Q(user=profile_user),
                ~Q(~Q(date__startswith=today), Q(score=None))
                )

    def get_context_data(self, *args, **kwargs):
        context = super(QuizBookList, self).get_context_data(
                *args, **kwargs)

        user = self.request.user
        profile_user = get_object_or_404(UserProfile,
              screen_name=self.kwargs['screen_name'],
              )
        op_user = user if not user.is_anonymous() else None

        is_my_tutor = Ring.objects.filter(
                tutor=profile_user, student=op_user, status='accepted'
                ).exists()
        is_my_student = Ring.objects.filter(
                student=profile_user, tutor=op_user, status='accepted'
                ).exists()
        ring = Ring.objects.filter(
                tutor=profile_user, student=op_user
                )
        ring = ring[0] if ring.exists() else None
        is_follower = Ring.objects.filter(
                student=profile_user, tutor=op_user, follow=True
                ).exists()

        today = datetime.today().strftime("%Y-%m-%d")
        next_quiz_date = today
        latest_quiz_date = today

        if self.get_queryset().filter(date__startswith=today):
            latest_quiz_date = self.get_queryset().filter(
                    date__startswith=today).latest('date').date
            if len(latest_quiz_date.split('-')) > 3:
                next_quiz_date = "%s-%d" % (
                        today, int(latest_quiz_date.split('-')[3]) + 1)
            else:
                next_quiz_date = "%s-1" % today

        context.update({
                'profile_user': profile_user,
                'is_my_tutor': is_my_tutor,
                'is_my_student': is_my_student,
                'is_follower': is_follower,
                'ring': ring,
                'today': today,
                'latest_quiz_date': latest_quiz_date,
                'next_quiz_date': next_quiz_date,
                })
        return context


class QuizBookUpdate(DetailView, LangExtraContext):
    model = QuizBook
    context_object_name = 'quizbook'
    template_name = 'lang/quizbook/update.html'
    slug_field = 'date'
    slug_url_kwarg = 'date'

    def dispatch(self, request, *args, **kwargs):
        screen_name=self.kwargs['screen_name']
        if request.user.screen_name != screen_name:
            return HttpResponseRedirect(reverse('quizbook_list', 
                    args=(screen_name,)))

        object = self.get_object()
        date = self.kwargs['date']

        if object and object.score:
            return HttpResponseRedirect(reverse('quizbook_detail',
                    args=(screen_name, date)))
        elif object and object.quiz_set.all():
            return super(QuizBookUpdate, self).dispatch(
                    request, *args, **kwargs)
        return HttpResponseRedirect(reverse('quizbook_list', 
                args=(screen_name,)))

    def get_object(self):
        date = self.kwargs['date']
        user = UserProfile.objects.get(
                screen_name=self.kwargs['screen_name'])
        now = datetime.today()
        if date.startswith(now.strftime("%Y-%m-%d")):
            book, created = QuizBook.objects.get_or_create(user=user, date=date)
            if created and date != now.strftime("%Y-%m-%d"):
                log = user.point.lose_point('get_quiz', quizbook=book)
            return book
        else:
            if QuizBook.objects.filter(user=user, date=date).exists():
                book = QuizBook.objects.get(user=user, date=date)
                return book
            return None

    def get_context_data(self, *args, **kwargs):
        context = super(QuizBookUpdate, self).get_context_data(*args, **kwargs)
        profile_user = get_object_or_404(UserProfile,
                screen_name=self.kwargs['screen_name'],
                )
        context.update({
            'profile_user': profile_user,
            'date': self.kwargs['date'],
            })
        return context

    def get(self, request, *args, **kwargs):
        query = dict(request.POST)
        query.pop('csrfmiddlewaretoken', None)
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        context.update({
            'checked_sentences': map(lambda v: int(v[0]), query.values()),
            })
        return render_to_response(self.template_name, context, context_instance=RequestContext(request))

    @transaction.commit_on_success
    def post(self, request, *args, **kwargs):
        date = self.kwargs['date']
        screen_name = self.kwargs['screen_name']
        values = self.request.POST
        quizbook = self.get_object()
        quiz_count = quizbook.quiz_set.all().count()
        answer_count = 0

        try:
            for key, answer in values.iteritems():
                if key.startswith('quiz_'):
                    answer_count += 1
                    quiz_id = key.split('_')[1]
                    quiz = quizbook.get_quiz(quiz_id)
                    if quiz.correct_answer.id == int(answer):
                        quiz.answer = True
                        quiz.save()
            if answer_count != quiz_count:
                raise ValidationError('Must choice all answer')
            correct_answers = quizbook.quiz_set.filter(answer=True)
            quizbook.score = 100 * correct_answers.count() / quiz_count
            quizbook.save()

        except ValidationError:
            messages.warning(
                request, ugettext_lazy('You should answer all questions'))
            return self.get(request, args, kwargs)

        except: 
            messages.warning(
                request, ugettext_lazy('Something wrong in submitting quiz'))
            return self.get(request, args, kwargs)

        return HttpResponseRedirect(reverse(
            'quizbook_detail', args=(screen_name, date)))


class QuizBookDetail(DetailView, LangExtraContext):
    model = QuizBook
    context_object_name = 'quizbook'
    template_name = 'lang/quizbook/detail.html'
    slug_field = 'date'
    slug_url_kwarg = 'date'

    def dispatch(self, request, *args, **kwargs):
        object = self.get_object()
        date = self.kwargs['date']
        screen_name=self.kwargs['screen_name']

        if object and object.score:
            return super(QuizBookDetail, self).dispatch(
                request, *args, **kwargs)
        elif object and object.quiz_set.all().count():
            return HttpResponseRedirect(reverse('quizbook_update',
                    args=(screen_name, date)))
        return HttpResponseRedirect(reverse('quizbook_list', 
                args=(screen_name,)))

    def get_object(self):
        date = self.kwargs['date']
        user = UserProfile.objects.get(
                screen_name=self.kwargs['screen_name'])
        now = datetime.today()
        if now.strftime("%Y-%m-%d") == date:
            book, created = QuizBook.objects.get_or_create(user=user, date=date)
            return book
        else:
            if QuizBook.objects.filter(user=user, date=date).exists():
                book = QuizBook.objects.get(user=user, date=date)
                return book
            return None


class QuizFeedbackForm(forms.ModelForm):

    class Meta:
        model = Quiz
        fields = ('feedback',)
        #exclude = ('question', 'quizbook', 'hint_open_count', 'correct_answer')


class QuizFeedback(UpdateView):
    model = Quiz
    form_class = QuizFeedbackForm

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(QuizFeedback, self).dispatch(
                request, *args, **kwargs)

    def render_to_json_response(self, context, **response_kwargs):
        data = json.dumps(context)
        response_kwargs['content_type'] = 'application/json'
        return HttpResponse(data, **response_kwargs)

    def form_valid(self, form):
        quiz = form.save()
        return self.render_to_json_response({'status': 'success'}, status=200)

    def form_invalid(self, form):
        return self.render_to_json_response(form.errors, status=400)

