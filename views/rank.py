from django.db import transaction
from django.db.models import Q
from django.shortcuts import render_to_response, get_object_or_404

from django.views.generic import ListView
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext_lazy
from django.utils.decorators import method_decorator

from lang.models import RankPeriod, Rank
from lang.views import LangExtraContext

from datetime import date, timedelta
from isoweek import Week


class WeeklyRankList(ListView, LangExtraContext):
    model = Rank
    context_object_name = 'rank_list'
    template_name = 'lang/rank/weekly_list.html'
    paginate_by = 50

    def get_queryset(self):
        today = date.today()
        previous_week_year = today.year if today.month != 1 else today.year - 1
        previous_week = today.isocalendar()[1] - 1

        rank_period = RankPeriod.objects.filter(
                week=previous_week, year=previous_week_year)
        ranks = Rank.objects.filter(period__in=rank_period).distinct()
        return ranks

    def get_context_data(self, *args, **kwargs):
        context = super(WeeklyRankList, self).get_context_data(
                *args, **kwargs)
        user = self.request.user
        today = date.today()
        previous_week_year = today.year if today.month != 1 else today.year - 1
        previous_week = today.isocalendar()[1] - 1

        w = Week(previous_week_year, previous_week)
        rank_period = RankPeriod.objects.get(
                language=user.get_learning_language(),
                week=previous_week, year=previous_week_year
                )
        learning_ranks = Rank.objects.filter(period=rank_period).distinct()

        rank_period = RankPeriod.objects.get(
                language=user.get_native_language(),
                week=previous_week, year=previous_week_year
                )
        native_ranks = Rank.objects.filter(period=rank_period).distinct()

        queryset = self.get_queryset()
        if self.get_queryset().filter(user=user).exists():
            my_point = self.get_queryset().filter(user=user)[0]
            my_rank = queryset.filter(points__gt=my_point.points).count() + 1
            my_native_rank = native_ranks.filter(points__gt=my_point.points).count() + 1
        else:
            my_rank = None
            my_native_rank = None
            my_point = None

        context.update({
            'learning_ranks': learning_ranks[:50],
            'native_ranks': native_ranks[:50],
            'period': "%s ~ %s" % (
                w.monday().strftime("%m. %d. %Y"),
                w.sunday().strftime("%m. %d. %Y")
                ),
            'my_point': my_point,
            'my_rank': my_rank,
            'my_native_rank': my_native_rank
            })
        return context


class MonthlyRankList(ListView, LangExtraContext):
    model = Rank
    context_object_name = 'rank_list'
    template_name = 'lang/rank/monthly_list.html'
    paginate_by = 50

    def get_queryset(self):
        today = date.today()
        previous_month_year = today.year if today.month != 1 else today.year - 1
        previous_month =  today.month - 1 if today.month != 1 else 12
        rank_period = RankPeriod.objects.filter(
                month=previous_month, year=previous_month_year)
        ranks = Rank.objects.filter(period__in=rank_period).distinct()
        return ranks

    def get_context_data(self, *args, **kwargs):
        context = super(MonthlyRankList, self).get_context_data(
                *args, **kwargs)
        user = self.request.user
        today = date.today()
        previous_month_year = today.year if today.month != 1 else today.year - 1
        previous_month =  today.month - 1 if today.month != 1 else 12

        rank_period = RankPeriod.objects.get(
                language=user.get_learning_language(),
                month=previous_month, year=previous_month_year
                )
        learning_ranks = Rank.objects.filter(period=rank_period).distinct()

        rank_period = RankPeriod.objects.get(
                language=user.get_native_language(),
                month=previous_month, year=previous_month_year
                )
        native_ranks = Rank.objects.filter(period=rank_period).distinct()

        queryset = self.get_queryset()
        if self.get_queryset().filter(user=user).exists():
            my_point = self.get_queryset().filter(user=user)[0]
            my_rank = queryset.filter(points__gt=my_point.points).count() + 1
            my_native_rank = native_ranks.filter(points__gt=my_point.points).count() + 1
        else:
            my_rank = None
            my_native_rank = None
            my_point = None

        import calendar
        r = calendar.monthrange(previous_month_year, previous_month)
        context.update({
            'learning_ranks': learning_ranks[:50],
            'native_ranks': native_ranks[:50],
            'period': "%s ~ %s" % (
                date(previous_month_year, previous_month, 1).strftime("%m. %d. %Y"),
                date(previous_month_year, previous_month, r[1]).strftime("%m. %d. %Y")
                ),
            'my_point': my_point,
            'my_rank': my_rank,
            'my_native_rank': my_native_rank
            })
        return context


class AllRankList(ListView, LangExtraContext):
    model = Rank
    context_object_name = 'rank_list'
    template_name = 'lang/rank/all_list.html'
    paginate_by = 50

    def get_queryset(self):
        rank_period = RankPeriod.objects.filter(week=None, month=None)
        ranks = Rank.objects.filter(period__in=rank_period).distinct()
        return ranks

    def get_context_data(self, *args, **kwargs):
        context = super(AllRankList, self).get_context_data(
                *args, **kwargs)
        user = self.request.user

        rank_period = RankPeriod.objects.get(
                language=user.get_learning_language(), week=None, month=None)
        learning_ranks = Rank.objects.filter(period=rank_period).distinct()

        rank_period = RankPeriod.objects.get(
                language=user.get_native_language(), week=None, month=None)
        native_ranks = Rank.objects.filter(period=rank_period).distinct()

        queryset = self.get_queryset()
        if self.get_queryset().filter(user=user).exists():
            my_point = self.get_queryset().filter(user=user)[0]
            my_rank = queryset.filter(points__gt=my_point.points).count() + 1
            my_native_rank = native_ranks.filter(points__gt=my_point.points).count() + 1
        else:
            my_rank = None
            my_native_rank = None
            my_point = None

        context.update({
            'learning_ranks': learning_ranks[:50],
            'native_ranks': native_ranks[:50],
            'period': "%s ~ %s" % (
                date(2013, 7, 14).strftime("%m. %d. %Y"),
                (date.today() - timedelta(days=1)).strftime("%m. %d. %Y")
                ),
            'my_point': my_point,
            'my_rank': my_rank,
            'my_native_rank': my_native_rank
            })
        return context
