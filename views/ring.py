import json
from itertools import chain

from django import forms
from django.db import transaction
from django.db.models import Q, Count
from django.shortcuts import get_object_or_404
from django.views.generic import (CreateView, UpdateView,
        FormView, DeleteView, ListView, DetailView)
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse, reverse_lazy
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.utils.translation import ugettext_lazy
from django.utils.decorators import method_decorator

from lang.models import Ring, Notification, ChatRoom
from lang.views import (LangExtraContext, MessageMixin,
        JSONResponseMixin, AjaxableResponseMixin)


class RingList(ListView, LangExtraContext):
    model = Ring
    context_object_name = 'rings'
    template_name = 'lang/ring/list.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(RingList, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        rings = super(RingList, self).get_queryset()
        queryset = rings.filter(tutor=self.request.user, status='requested')
        return queryset

    def get_context_data(self, *args, **kwargs):
        context = super(RingList, self).get_context_data(*args, **kwargs)

        user = self.request.user
        notifications = Notification.objects.filter(
                type='ring', is_new=True, receiver=user)
        news = list(
                notifications.values('message', 'ring__status', 'id', 'ring__follow'))
        talks = user.my_chatrooms().filter(~Q(messages=None)).order_by('-mtime')
        context.update({
                'talks': talks,
                'news': news,
                })
        return context


class RingCreate(JSONResponseMixin, CreateView):
    model = Ring

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(RingCreate, self).dispatch(request, *args, **kwargs)

    def form_invalid(self, form):
        response = super(RingCreate, self).form_invalid(form)
        context = {}
        context.update({
            'status': 'error',
            'message': form.errors,
            })
        if self.request.is_ajax():
            return JSONResponseMixin.render_to_response(self, context)
        else:
            return response

    def form_valid(self, form):
        user = self.request.user
        ring = form.save()

        context = {}
        context.update({
            'status': 'success',
            })
        if self.request.is_ajax():
            return JSONResponseMixin.render_to_response(self, context)
        else:
            return HttpResponseRedirect(reverse('ring_list'))


class RingUpdateForm(forms.ModelForm):
    class Meta:
        model = Ring
        exclude = ('tutor', 'student', )


class RingUpdate(UpdateView, JSONResponseMixin):
    model = Ring
    form_class = RingUpdateForm

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(RingUpdate, self).dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        try:
            return Ring.objects.get(pk=self.kwargs['pk'])
        except Ring.DoesNotExist:
            return None

    def post(self, request, *args, **kwargs):
        if not self.get_object():
            return HttpResponseRedirect(reverse('ring_list'))

        if self.request.is_ajax():
            ring = super(RingUpdate, self).post(request, *args, **kwargs)
            context = {}
            context.update({
                'status': 'success',
                })
            return JSONResponseMixin.render_to_response(self, context)
        return super(RingUpdate, self).post(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('ring_list')


class RingDelete(JSONResponseMixin, DeleteView):
    model = Ring
    success_url = reverse_lazy('ring_list')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(RingDelete, self).dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        try:
            return Ring.objects.get(pk=self.kwargs['pk'])
        except Ring.DoesNotExist:
            return None

    def post(self, *args, **kwargs):
        ring = self.get_object()
        user = self.request.user
        if ring:
            status = self.request.POST.get('status', '')

            if status == 'cancel':
                if user == ring.tutor:
                    receiver = ring.student
                    sender = ring.tutor
                else:
                    receiver = ring.tutor
                    sender = ring.student

                notification = Notification.objects.filter(
                        type__in=['ring', 'talk'],
                        receiver=receiver, sender__in=[sender])
                notification.update(is_new=False)
            else:
                notification, created = Notification.objects.get_or_create(
                        type='ring', receiver=ring.student)
                notification.sender.add(ring.tutor)
                notification.message = "Your recent request to \"%s\" has been rejected." % ring.tutor
                notification.is_new = True
                notification.save()

            ring.delete()

            # check exist other ring reversed tutor and student
            reversed_ring = Ring.objects.filter(
                    tutor=ring.student, student=ring.tutor, status='accepted'
                    )
            # delete chat history
            if not reversed_ring.exists():
                talks = ChatRoom.objects.filter(
                        member__in=[ring.tutor]).filter(
                        member__in=[ring.student]).distinct()
                talks.delete()

            context = {}
            context.update({
                'status': 'success',
                })
        else:
            context = {}

        if self.request.is_ajax():
            return JSONResponseMixin.render_to_response(self, context)
        else:
            return HttpResponseRedirect(reverse('ring_list'))
