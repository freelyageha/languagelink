import json
from itertools import chain

from django.core.exceptions import PermissionDenied
from django.views.generic import DeleteView
from django.views.generic.base import RedirectView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.core.urlresolvers import reverse
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django import forms
from django.db.models import Q

from lang.models import ChatRoom, Message, UserProfile, Notification
from lang.views import LangExtraContext


class TalkRedirection(RedirectView):
    permanent = False

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(TalkRedirection, self).dispatch(
                request, *args, **kwargs)

    def get_redirect_url(self):
        owner = self.request.user
        receiver = UserProfile.objects.get(id=self.request.GET['receiver'])

        is_my_tutor = owner.my_tutors().filter(tutor=receiver).exists()
        is_my_student = owner.my_students().filter(student=receiver).exists()

        accept = self.request.GET.get('accept', "")
        if owner == receiver or not (is_my_tutor or is_my_student):
            raise Http404

        try:
            chatroom = ChatRoom.objects.filter(
                    member=owner).get(member=receiver)
        except:
            chatroom = ChatRoom.objects.create()
            chatroom.member.add(owner)
            chatroom.member.add(receiver)

        if accept:
            admin = UserProfile.objects.get(username='LingRing')
            '''
            msg = Message.objects.create(
                    context="made a ring",
                    user=admin, room=chatroom, read_count=2
                    )
            '''

        return reverse('open_chatroom', args=(chatroom.id, ))


class TalkList(ListView, LangExtraContext):
    model = ChatRoom
    context_object_name = 'chatroom_list'
    template_name = 'lang/talk/chatroom_list.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(TalkList, self).dispatch(
                request, *args, **kwargs)

    def get_queryset(self, *args, **kwargs):
        new_queryset = ChatRoom.objects.filter(
                member__in=[self.request.user],
                notification__is_new=True).distinct()
        old_queryset = ChatRoom.objects.filter(
                Q(member__in=[self.request.user]),
                ~Q(notification__is_new=True)).distinct()
        queryset = list(chain(new_queryset, old_queryset))

        return queryset

    def get_context_data(self, *args, **kwargs):
        context = super(TalkList, self).get_context_data(
                *args, **kwargs)
        context.update({
            'noti_params': '?filter=chatroom',
            })
        return context



class TalkOpen(ListView, LangExtraContext):

    model = Message
    context_object_name = 'message_list'
    template_name = 'lang/talk/open.html'
    paginate_by = 20

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        user = self.request.user
        try:
            chatroom = ChatRoom.objects.get(id=self.kwargs['pk'])
        except:
            return HttpResponseRedirect(reverse('ring_list'))

        if user in chatroom.members():
            return super(TalkOpen, self).dispatch(
                    request, *args, **kwargs)
        raise PermissionDenied()

    def get_queryset(self, *args, **kwargs):
        queryset = Message.objects.filter(room__id=self.kwargs['pk'])
        return queryset

    def get_context_data(self, *args, **kwargs):
        context = super(TalkOpen, self).get_context_data(*args, **kwargs)

        chatroom = ChatRoom.objects.get(id=self.kwargs['pk'])
        user = self.request.user

        try:
            notification = Notification.objects.get(
                    talk=chatroom, type='talk', receiver=user,
                    is_new = True,
                    )
            notification.is_new=False
            notification.save()
        except:
            pass
 
        context.update({
            'chatroom': chatroom,
            'noti_params': '?filter=talking&room_id=' + str(chatroom.id),
            })
        return context


class MessageForm(forms.ModelForm):

    class Meta:
        model = Message
        exclude = ('user', 'ip_addr', 'read_count')

    def __init__(self, *args, **kwargs):
        super(MessageForm, self).__init__(*args, **kwargs)
        room = self.fields.get('room')
        room.widget = forms.HiddenInput()


class TalkSendMessage(CreateView, LangExtraContext):
    model = Message
    form_class = MessageForm

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(TalkSendMessage, self).dispatch(
                request, *args, **kwargs)

    def render_to_json_response(self, context, **response_kwargs):
        data = json.dumps(context)
        response_kwargs['content_type'] = 'application/json'
        return HttpResponse(data, **response_kwargs)

    def form_invalid(self, form):
        if self.request.is_ajax():
            return self.render_to_json_response(form.errors, status=400)
        else:
            return super(TalkSendMessage, self).form_invalid(form)

    def form_valid(self, form):
        response = super(TalkSendMessage, self).form_valid(form)
        user = self.request.user
        msg = form.save(commit=False)
        msg.user = user
        msg.save()
        msg.room.save()

        for member in msg.room.members().exclude(id=user.id):
            notification, created = Notification.objects.get_or_create(
                    type='talk', talk=msg.room,
                    sender__in=[user], receiver=member)
            notification.is_new=True
            notification.sender.add(user)
            notification.save()

        if self.request.is_ajax():
            data = {
                'context': self.object.context,
                'ctime': self.object.ctime.strftime("%H:%I:%S")
            }
            return self.render_to_json_response(data)
        else:
            return HttpResponseRedirect(
                    reverse('open_chatroom', args=(msg.room,)))


class TalkQuit(DeleteView, LangExtraContext):
    model = ChatRoom

    def get_success_url(self):
        return reverse('ring_list')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(TalkQuit, self).dispatch(
                request, *args, **kwargs)

    def render_to_json_response(self, context, **response_kwargs):
        data = json.dumps(context)
        response_kwargs['content_type'] = 'application/json'
        return HttpResponse(data, **response_kwargs)

    def delete(self, request, *args, **kwargs):
        room = self.get_object()
        has_notification = room.notification.filter(
                receiver=self.request.user, is_new=True).exists()
        room.delete()

        if self.request.is_ajax():
            data = { 'status': 'success', 'has_notification': has_notification }
            return self.render_to_json_response(data)
        else:
            return HttpResponseRedirect(reverse('ring_list'))
