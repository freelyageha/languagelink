import json

from datetime import datetime

from django import forms
from django.http import Http404
from django.db import transaction
from django.db.models import Q
from django.shortcuts import render_to_response, get_object_or_404
from django.views.generic import CreateView, UpdateView, ListView, DetailView, DeleteView, FormView
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.core.exceptions import ValidationError
from django.contrib import messages
from django.contrib.auth.decorators import login_required

from django.template import RequestContext
from django.utils.translation import ugettext_lazy
from django.utils.decorators import method_decorator

from lang.models import Topic, UserProfile, Language
from lang.views import LangExtraContext, MessageMixin, AuthorRequiredMixin, JSONResponseMixin
from lang.decorators import staff_required


class TopicList(ListView, LangExtraContext):
    model = Topic
    context_object_name = 'topics'
    template_name = 'lang/topic/list.html'
    paginate_by = 10

    def dispatch(self, request, *args, **kwargs):
        return super(TopicList, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        queryset = super(TopicList, self).get_queryset()
        language = get_object_or_404(Language, name=self.kwargs['language'], )
        queryset = queryset.filter(language=language)
        return queryset

    def get_context_data(self, *args, **kwargs):
        context = super(TopicList, self).get_context_data(**kwargs)
        context.update({
            'language': self.kwargs['language'],
            'ask_of_topics': self.request.user.ask_set.filter(
                topic__in=self.get_queryset()).values_list('topic__id', flat=True)
            })
        return context


class TopicForm(forms.ModelForm):

    class Meta:
        model = Topic
        exclude = ('user', 'language', 'ip_addr', )


class TopicCreate(CreateView, LangExtraContext, MessageMixin):
    model = Topic
    form_class = TopicForm
    template_name='lang/topic/list.html'
    success_message = "Correction Submit Successfully"

    @method_decorator(staff_required)
    def dispatch(self, request, *args, **kwargs):
        return super(TopicCreate, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(TopicCreate, self).get_context_data(**kwargs)
        context['language']=self.kwargs['language']
        return context

    def form_invalid(self, form):
        messages.set_level(self.request, messages.ERROR)
        messages.error(self.request, "%s" % form.errors)
        return HttpResponseRedirect(reverse('topic_list', args=(self.kwargs['language'], )))

    def form_valid(self, form):
        user = self.request.user
        topic = form.save(commit=False)
        topic.user = user
        topic.language = user.get_native_language()
        topic.save()

        messages.success(self.request, self.success_message)
        return super(TopicCreate, self).form_valid(form)

    def get_success_url(self):
        return reverse('topic_list', args=(self.kwargs['language'],))


class TopicUpdate(UpdateView, AuthorRequiredMixin, LangExtraContext, MessageMixin):
    model = Topic
    form_class = TopicForm
    context_object_name = 'topic'
    template_name='lang/topic/edit.html'
    success_message = "Update Submit Successfully"

    @method_decorator(staff_required)
    def dispatch(self, request, *args, **kwargs):
        return super(TopicUpdate, self).dispatch(request, *args, **kwargs)

    def form_invalid(self, form):
        messages.set_level(self.request, messages.ERROR)
        messages.error(self.request, "%s" % form.errors)
        return HttpResponseRedirect(reverse('topic_list', args=(self.kwargs['language'], )))

    def form_valid(self, form):
        user = self.request.user
        topic = form.save(commit=False)
        topic.user = user
        topic.language = user.get_native_language()
        topic.save()

        messages.success(self.request, self.success_message)

        return HttpResponseRedirect(reverse('topic_list', args=(self.kwargs['language'],)))


class TopicDelete(DeleteView, AuthorRequiredMixin):
    model = Topic

    @method_decorator(staff_required)
    def dispatch(self, request, *args, **kwargs):
        return super(TopicDelete, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('topic_list', args=(self.kwargs['language'],))


class TopicAddReader(JSONResponseMixin, UpdateView):
    model = Topic

    def dispatch(self, request, *args, **kwargs):
        return super(TopicAddReader, self).dispatch(request, *args, **kwargs)

    def post(self, *args, **kwargs):
        try:
            topic = self.get_object()
            user = self.request.user
            topic.reader.add(user)
            context = {
                'status': 'success',
                'topic': False
                }
            if user.has_unread_topics():
                topic = user.unread_topics()[0]
                context.update({
                    'topic': True,
                    'id': topic.id,
                    'title': topic.title,
                    'author': topic.user.screen_name,
                    'author_language': topic.language.name,
                    'add_reader_url': reverse('topic_add_reader', args=(topic.language.name, topic.id,))
                    })
            else:
                context.update({
                    'topic_list_url': reverse('topic_list', args=(user.get_learning_language().name,))
                    })
            return JSONResponseMixin.render_to_response(self, context)
        except:
            raise Http404
