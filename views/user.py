import json

from django.views.generic.base import RedirectView, TemplateView
from django.views.generic.edit import UpdateView, CreateView
from django.views.generic.detail import DetailView
from django.core.urlresolvers import reverse
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django import forms
from django.utils.translation import ugettext_lazy
from django.core.exceptions import ValidationError
from django.db import IntegrityError
from django.forms.util import ErrorList
from django.http import HttpResponseRedirect, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import get_object_or_404

from emailconfirmation.models import EmailAddress, EmailConfirmation

from lang.models import UserProfile, SNS, Language, Point
from lang.views import LangExtraContext, JSONResponseMixin
from lang.choices import EMAIL_HOST_CHOICE
from lang.decorators import newbie_required


class ProfileRedirect(RedirectView):
    permanent = True

    def get_redirect_url(self):
        user = self.request.user
        if not user.is_authenticated():
            return reverse('login')
        elif user.is_new or not user.screen_name:
            return reverse('profile_setting', args=(user.id, ))
        return reverse('ask_language_list', args=(user.get_native_language().name,))


class ProfileForm(forms.ModelForm):
    native_language = forms.ModelChoiceField(
            label=ugettext_lazy("Native Language"),
            required=True,
            queryset=Language.objects.all()
            )
    learning_language = forms.ModelChoiceField(
            label=ugettext_lazy("Learning Language(Language I want to learn)"),
            required=True,
            queryset=Language.objects.all(),
            error_messages = {
                'invalid': ugettext_lazy('You must choose different languages')}
            )

    class Meta:
        model = UserProfile
        fields = ('email',)

    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        current_native_language = \
                self.instance.get_native_language()
        current_learning_language = \
                self.instance.get_learning_language()
        native_language = self.fields.get('native_language')
        native_language.initial = current_native_language
        learning_language = self.fields.get('learning_language')
        learning_language.initial = current_learning_language
        email = self.fields.get('email')
        email.required = True

    def clean_learning_language(self):
        learning_language = self.cleaned_data.get('learning_language')
        native_language = self.cleaned_data.get('native_language')
        if learning_language == native_language:
            raise ValidationError(self.fields['learning_language'].error_messages['invalid'])
        return learning_language

    def save(self, *args, **kwargs):
        profile = super(ProfileForm, self).save(*args, **kwargs)
        native_language = self.cleaned_data.get('native_language')
        learning_language = self.cleaned_data.get('learning_language')

        profile.native_language.clear()
        profile.native_language.add(native_language)
        profile.learning_language.clear()
        profile.learning_language.add(learning_language)

        if profile.is_new:
            profile.is_new = False
        profile.save()
        profile.set_level()
        point, created = Point.objects.get_or_create(user=profile)
        point_log, grade = profile.point.acquire_point('join', None)
        return profile


class ProfileSettingForm(ProfileForm):

    class Meta:
        model = UserProfile
        fields = ('screen_name','email', 'location', 'description',)

    def is_valid(self):
        is_valid = super(ProfileSettingForm, self).is_valid()
        try:
            screen_name = self.cleaned_data['screen_name']
        except KeyError:
            self._errors["screen_name"] = \
                ErrorList([ugettext_lazy("Must insert ID")])
            return False

        import re
        if UserProfile.objects.filter(screen_name=screen_name).exists():
            self._errors["screen_name"] = \
                ErrorList([ugettext_lazy("Already exist")])
            return False

        if re.sub('[a-zA-Z0-9_-]', '', screen_name):
            self._errors["screen_name"] = \
                ErrorList([ugettext_lazy(
                    "Only use alphabet, numbers, '-' and '_'")])
            return False
        return is_valid


class ProfileSetting(UpdateView):
    model = UserProfile
    form_class = ProfileSettingForm
    context_object_name = 'profile'
    template_name='lang/user/create.html'

    @method_decorator(login_required)
    @method_decorator(newbie_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ProfileSetting, self).dispatch(
                request, *args, **kwargs)

    def form_valid(self, form):
        profile = form.save()

        try:
            email_address = EmailAddress.objects.create(
                    user=profile, email=profile.email)
        except IntegrityError:
            email_address = None

        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        grade = self.request.user.point.grade()
        redirect_url = reverse('example')
        redirect_url += '?grade=' + grade
        return redirect_url


class ProfileUpdateForm(ProfileForm):

    class Meta:
        model = UserProfile
        fields = (
                'email', 'location', 'description',
                'subscribe_correction',
                'subscribe_reply',
                'subscribe_ring_request',
                'subscribe_student_post',
                )


class ProfileUpdate(UpdateView):
    model = UserProfile
    form_class = ProfileUpdateForm
    context_object_name = 'profile'
    template_name='lang/user/update.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if self.request.user != self.get_object():
            return HttpResponseRedirect(reverse('ask_language_list'))
        return super(ProfileUpdate, self).dispatch(
                request, *args, **kwargs)

    def form_valid(self, form):
        current_email = self.request.user.email
        new_email = form.cleaned_data.get('email')
        if new_email != current_email:
            profile = form.save()
            emailaddress = profile.emailaddress_set.filter(email=new_email)
            if not emailaddress.exists():
                email_address = EmailAddress.objects.add_email(
                        profile, profile.email)
            elif emailaddress.filter(verified=False).exists():
                emailaddress.filter(verified=False).delete()
                email_address = EmailAddress.objects.add_email(
                        profile, profile.email)
        return super(ProfileUpdate, self).form_valid(form)


class SNSUpdateForm(forms.ModelForm):
    message = forms.CharField(
            label=ugettext_lazy("Message for your student"),
            required=False,
            widget=forms.Textarea(attrs={'rows':4})
            )

    class Meta:
        model = SNS


class SNSCreate(CreateView):
    model = SNS
    form_class = SNSUpdateForm
    context_object_name = 'profile'
    template_name='lang/user/sns_update.html'

    def get_object(self):
        user = get_object_or_404(UserProfile, id=self.kwargs['pk'])
        return user

    def form_valid(self, form):
        sns = form.save()
        user = self.request.user
        user.sns = sns
        user.save()

        return HttpResponseRedirect(
                reverse('ask_post_list', args=(user.screen_name,)))


class SNSUpdate(UpdateView):
    model = SNS
    form_class = SNSUpdateForm
    context_object_name = 'sns'
    template_name='lang/user/sns_update.html'

    def get_object(self):
        profile = get_object_or_404(UserProfile, id=self.kwargs['pk'])
        return profile.sns

    def get_context_data(self, *args, **kwargs):
        context = super(SNSUpdate, self).get_context_data(
                *args, **kwargs)
        profile = get_object_or_404(
                UserProfile, id=self.kwargs['pk'])
        context.update({
            'profile': profile,
            })
        return context

    def form_valid(self, form):
        sns = form.save()
        user = self.request.user
        return HttpResponseRedirect(
                reverse('ask_post_list', args=(user.screen_name,)))



class ProfileDetail(DetailView):
    model = UserProfile
    context_object_name = 'profile_user'
    template_name='lang/user/profile.html'


class SendConfirmEmail(JSONResponseMixin, TemplateView):
    template_name='emailconfirmation/email_confirmation_message.html'

    def render_to_response(self, context):
        user = self.request.user
        email_address = self.request.user.emailaddress_set.get(
                user=user, email=user.email)
        EmailConfirmation.objects.send_confirmation(email_address)
        context = {}
        context.update({
            'status': 'success',
            })
        return JSONResponseMixin.render_to_response(
            self, context)


class SentConfirmEmail(TemplateView):

    template_name = "emailconfirmation/sent_confirm_email.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(SentConfirmEmail, self).dispatch(
                request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(SentConfirmEmail, self).get_context_data(
                *args, **kwargs)
        email = self.request.user.email
        email_host = '#'
        if EMAIL_HOST_CHOICE.has_key(email.split("@")[1]):
            email_host = EMAIL_HOST_CHOICE[email.split("@")[1]]
        context.update({
            'email_host': email_host,
            'email': email,
            })
        return context


class OfftheNewbieFlag(TemplateView):
    template_name = "registration/offthenewbieflag.html"

    @method_decorator(login_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(OfftheNewbieFlag, self).dispatch(
                request, *args, **kwargs)

    def render_to_json_response(self, context, **response_kwargs):
        data = json.dumps(context)
        response_kwargs['content_type'] = 'application/json'
        return HttpResponse(data, **response_kwargs)

    def post(self, request, *args, **kwargs):
        user = self.request.user
        user.is_newbie = False
        user.save()
        return self.render_to_json_response({})


class EmailLogin(TemplateView):
    def post(self, request, *args, **kwargs):
        email_id = request.POST.get('email')
        email = EmailAddress.objects.get(id=email_id)
        user = UserProfile.objects.get(id=email.user.id)

        from django.contrib.auth import authenticate, login
        user.backend = 'django.contrib.auth.backends.ModelBackend'
        login(request, user)
        #return HttpResponseRedirect(reverse('home'))


